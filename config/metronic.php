<?php

return [

    /**
     * Admin application namespace
     */
    'app_namespace' => "App\\MetronicAdmin",

    /**
     * Package work dirs
     */
    'paths' => [
        'app' => app_path('MetronicAdmin'),
        'view' => 'admin'
    ],

    /**
     * Global rout configurations
     */
    'route' => [
        'domain' => '',
        'prefix' => 'metronic',
        'name' => 'metronic.',
        'layout' => 'metronic_layout'
    ],

    /**
     * Default actions
     */
    'action' => [
        'auth' => [
            'login_form_action' => '\Lar\MetronicAdmin\Controllers\AuthController@login',
            'login_post_action' => '\Lar\MetronicAdmin\Controllers\AuthController@login_post'
        ],
        'profile' => [
            'index' => '\Lar\MetronicAdmin\Controllers\UserController@index',
            'update' => '\Lar\MetronicAdmin\Controllers\UserController@update',
            'logout' => '\Lar\MetronicAdmin\Controllers\UserController@logout'
        ],
        'dashboard' => '\Lar\MetronicAdmin\Controllers\DashboardController@index',
        'uploader' => '\Lar\MetronicAdmin\Controllers\UploadController@index',
    ],

    /**
     * Authentication settings for all lar admin pages. Include an authentication
     * guard and a user provider setting of authentication driver.
     */
    'auth' => [

        'guards' => [
            'metronic' => [
                'driver'   => 'session',
                'provider' => 'metronic',
            ],
        ],

        'providers' => [
            'metronic' => [
                'driver' => 'eloquent',
                'model'  => \Lar\MetronicAdmin\Models\MetronicUser::class,
            ],
        ],
    ],

    /**
     * Admin metronic upload setting
     *
     * File system configuration for form upload files and images, including
     * disk and upload path.
     */
    'upload' => [

        'disk' => 'metronic',

        /**
         * Image and file upload path under the disk above.
         */
        'directory' => [
            'image' => 'images',
            'file'  => 'files',
        ],
    ],

    /**
     * Admin metronic use disks
     */
    'disks' => [
        'metronic' => [
            'driver' => 'local',
            'root' => public_path('uploads'),
            'visibility' => 'public',
            'url' => env('APP_URL').'/uploads',
        ]
    ],

    'footer' => [
        'copy' => '<strong>Copyright &copy; '.date('Y').'.</strong> All rights reserved.'
    ],

    'lang_flags' => [
        'uk' => 'flag-icon flag-icon-ua',
        'en' => 'flag-icon flag-icon-us',
        'ru' => 'flag-icon flag-icon-ru',
    ]
];
