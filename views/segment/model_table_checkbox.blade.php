@if (!$id)
    <div class="btn-group">
        <button type="button" class="btn btn-link p-0">
            <div class="icheck-primary">
                <input type="checkbox"
                       name="select_{{$table_id}}"
                       id="{{$nId = "select_".$table_id . "_all"}}"
                       data-change="table_list::checkChildCheckboxes">
                <label for="{{$nId}}">
                </label>
            </div>
        </button>
    </div>
@else
    <div class="icheck-primary d-inline">
        <input type="checkbox"
               class="select_{{$table_id}}"
               name="select_{{$table_id}}[{{$id}}]"
               value="{{$id}}"
               @if($disabled) disabled="true" @endif
               id="{{$nId = $table_id . $id}}">
        <label for="{{$nId}}">
        </label>
    </div>
@endif
