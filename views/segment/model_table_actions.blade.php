<div class="dropdown dropdown-inline">
    <button type="button" class="btn btn-light-primary btn-icon btn-sm" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="ki ki-bold-more-ver"></i>
    </button>
    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
        @foreach($actions as $action)
            <a class="dropdown-item"
                    data-click="table_action"
                    data-table="{{$table_id}}"
                    data-object="{{$object}}"
                    data-columns="{{json_encode($columns, JSON_UNESCAPED_UNICODE)}}"
                    @if(isset($action['confirm']) && $action['confirm']) data-confirm="@lang($action['confirm'])" @endif
                    data-jax="{{$action['jax']}}"
                    type="button"
            >
                @if(isset($action['icon']) && $action['icon']) <i class="{{$action['icon']}}"></i>&nbsp;@endif
                @if(isset($action['title']) && $action['title']) @lang($action['title']) @endif
            </a>
        @endforeach
        @if($delete)
            <a class="dropdown-item"
                    data-click="table_action"
                    data-table="{{$table_id}}"
                    data-object="{{$object}}"
                    data-columns="{{json_encode($columns, JSON_UNESCAPED_UNICODE)}}"
                    data-confirm="@lang('metronic.delete_selected_rows')"
                    data-jax="metronic_admin.mass_delete"
                    type="button"
            ><i class="fas fa-trash"></i>&nbsp; @lang('metronic.delete')</a>
        @endif
    </div>
</div>
