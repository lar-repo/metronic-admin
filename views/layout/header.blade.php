<div id="kt_header" class="header header-fixed">
    <!--begin::Container-->
    <div class="container-fluid d-flex align-items-stretch justify-content-between">

        <div class="topbar header-menu-wrapper-left">
            <div class="topbar-item">
                <a class="nav-link" data-click="doc::back" title="@lang('metronic.back')" href="javascript:void(0)"><i class="fas fa-arrow-left"></i></a>
            </div>

            <div class="topbar-item">
                <a class="nav-link" data-click="doc::reload" title="@lang('metronic.refresh')" href="javascript:void(0)"><i class="fas fa-redo-alt"></i></a>
            </div>

            @if(admin()->isRoot())
                <div class="topbar-item">
                    <a class="nav-link" title="Root tools" data-toggle="collapse" href="#root-tools" role="button" aria-expanded="false" aria-controls="root-tools"><i class="fas fa-chess"></i></a>
                </div>
            @endif

            @foreach(gets()->metronic->menu->nested_collect->where('left_nav_bar_view') as $menu)
                @if(View::exists($menu['left_nav_bar_view']))
                    @include($menu['left_nav_bar_view'], $menu['params'])
                @else
                    {!! new $menu['left_nav_bar_view'](...$menu['params']); !!}
                @endif
            @endforeach

            {!! \Lar\MetronicAdmin\Components\Vue\LiveReloader::create() !!}
        </div>

        <!--begin::Header Menu Wrapper-->
        <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">

        </div>
        <!--end::Header Menu Wrapper-->
        <!--begin::Topbar-->
        <div class="topbar">

            @foreach(gets()->metronic->menu->nested_collect->where('badge')->where('link') as $menu)
                @php
                    $counter = isset($menu['badge']['instructions']) && $menu['badge']['instructions'] ?
                        eloquent_instruction($menu['badge']['text'], $menu['badge']['instructions'])->count() :
                        $menu['badge']['text'];
                    $link = isset($menu['link']) ? (isset($menu['badge']['params']) ? makeUrlWithParams($menu['link'], $menu['badge']['params']) : $menu['link']) : 'javascript:void(0)';
                @endphp
                @if($counter)
                    <li class="nav-item">
                        <a class="nav-link" href="{{$link}}" title="{{__($menu['badge']['title'] ?? $menu['title'])}}">
                            <i class="{{$menu['icon']}}"></i>
                            <span class="badge badge-{{isset($menu['badge']['type']) ? $menu['badge']['type'] : 'info'}} navbar-badge">{{$counter}}</span>
                        </a>
                    </li>
                @endif
            @endforeach

            @foreach(gets()->metronic->menu->nested_collect->where('nav_bar_view')->where('prepend', false) as $menu)
                @if(View::exists($menu['nav_bar_view']))
                    @include($menu['nav_bar_view'], $menu['params'])
                @else
                    {!! new $menu['nav_bar_view'](...$menu['params']); !!}
                @endif
            @endforeach
            <div class="topbar-item">
                <a class="nav-link" target="_blank" href="{{url('/')}}" title="{{__('metronic.open_homepage_in_new_tab')}}"><i class="fas fa-external-link-square-alt"></i></a>
            </div>

            <div class="topbar-item">
                <a class="nav-link" href="javascript:void(0)" data-click="alert::confirm" data-params="{{__('metronic.logout')}}, {{admin()->name}}? && {{route('metronic.profile.logout')}} >> $jax.get" title="{{__('metronic.logout')}}"><i class="fas fa-sign-out-alt"></i></a>
            </div>

            <!--begin::User-->
            <div class="topbar-item">
                <div data-click="doc::location" data-param="{{route('metronic.profile')}}" class="btn btn-icon btn-icon-mobile w-auto btn-clean d-flex align-items-center btn-lg px-2" id="kt_quick_user_toggle">
                    <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Hi,</span>
                    <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">{{admin()->name}}</span>
                    <span class="symbol symbol-lg-35 symbol-25 symbol-light-success">
                        <span class="symbol-label font-size-h5 font-weight-bold">{{mb_strcut(admin()->name, 0, 1)}}</span>
                    </span>
                </div>
            </div>
            <!--end::User-->

            @foreach(gets()->metronic->menu->nested_collect->where('nav_bar_view')->where('prepend', true) as $menu)
                @if(View::exists($menu['nav_bar_view']))
                    @include($menu['nav_bar_view'], $menu['params'])
                @else
                    {!! new $menu['nav_bar_view'](...$menu['params']); !!}
                @endif
            @endforeach
        </div>
        <!--end::Topbar-->
    </div>
    <!--end::Container-->
</div>