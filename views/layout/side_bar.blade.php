<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">

    <!-- Sidebar -->
    <div class="sidebar">
        <div class="sidebar-inner" @live('sidebar')>
            <!-- Sidebar user (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex" data-href="{{route('metronic.profile')}}">
                <div class="image">
                    <img src="{{asset(metronic_user()->avatar)}}" class="img-circle elevation-2" alt="{{metronic_user()->name}}">
                </div>
                <div class="info"  title="{{__('metronic.profile')}}">
                    <a class="d-block">{{metronic_user()->name}}</a>
                    <small class="d-block"><span class="badge badge-success">{!! MetronicAdmin::user()->roles->pluck('name')->implode('</span>, <span class="badge badge-success">') !!}</span></small>
                </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
                <ul class="nav nav-sidebar nav-child-indent flex-column" data-widget="treeview" role="menu" data-accordion="true">
                    @include('metronic::layout.side_bar_items')
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
    </div>
    <!-- /.sidebar -->
</aside>
