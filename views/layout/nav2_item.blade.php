@foreach(($items ?? gets()->metronic->menu->nested_collect->where('parent_id', 0)) as $menu)

    @php
        $access = isset($menu['roles']) ? metronic_user()->hasRoles($menu['roles']) : true;
        $childs = gets()->metronic->menu->nested_collect->where('parent_id', '!=', 0)->where('parent_id', $menu['id']);
        $on = $childs->count() ? !!$childs->where('active', true)->count() : true;
    @endphp

    @if($menu['active'] && $access && $on)

        @php
            $selected = $menu['selected'] || $childs->where('selected', true)->count();
        @endphp

        <li class="menu-item {{$selected && isset($nes) ? 'menu-item-active' : ''}} {{ $childs->count() ? 'menu-item-submenu' : '' }} {{ $childs->count() && $selected ? 'menu-item-open' : '' }}" aria-haspopup="true" @if($childs->count()) data-menu-toggle="hover" @endif >
            <a href="{{$menu['link'] && !$childs->count() ? $menu['link'] : 'javascript:void(0)'}}" @if($menu['target']) target="_blank" @endif class="menu-link {{ !$childs->count() ? ($selected ? 'menu-item-active' : '') : ( !isset($nes) && $selected ? 'menu-item-active' : '' ) }} {{ $childs->count() ? 'menu-toggle' : '' }}">
                @if (isset($menu['icon']) && !isset($items))
                    <span class="svg-icon menu-icon">
                        <i class="nav-icon {{$menu['icon']}}"></i>
                    </span>
                @elseif (isset($items))
                    <i class="menu-bullet menu-bullet-line">
                        <span></span>
                    </i>
                @endif

                <span class="menu-text">@lang($menu['title'])</span>

                @if (isset($menu['badge']) && is_array($menu['badge']))

                    <span id="nav_badge_{{isset($menu['badge']['id']) && $menu['badge']['id'] ? $menu['badge']['id'] : $menu['id']}}" class="menu-label" {!! isset($menu['badge']['title']) ? "title='{$menu['badge']['title']}'" : "" !!}>
                        <span class="label label-rounded label-{{isset($menu['badge']['type']) ? $menu['badge']['type'] : 'info'}}">
                            @if(isset($menu['badge']['instructions']) && $menu['badge']['instructions'])
                                {{eloquent_instruction($menu['badge']['text'], $menu['badge']['instructions'])->count()}}
                            @else
                                {{isset($menu['badge']['text']) ? __($menu['badge']['text']) : 0}}
                            @endif
                        </span>
                    </span>

                @elseif(isset($menu['badge']))

                    <span id="nav_badge_{{$menu['id']}}" class="menu-label">
                        <span class="label label-rounded label-info">@lang($menu['badge'])</span>
                    </span>

                @elseif($childs->count())

                    <i class="menu-arrow"></i>

                @endif
            </a>

            @if($childs->count())

                <div class="menu-submenu">
                    <i class="menu-arrow"></i>
                    <ul class="menu-subnav">
                        @include('metronic::layout.nav2_item', ['items' => $childs, 'nes' => true])
                    </ul>
                </div>

            @endif
        </li>

    @elseif(isset($menu['main_header']) && $menu['main_header'])
        <li class="menu-section">
            <h4 class="menu-text">@lang($menu['main_header'])</h4>
            <i class="menu-icon ki ki-bold-more-hor icon-md"></i>
        </li>
    @endif

@endforeach