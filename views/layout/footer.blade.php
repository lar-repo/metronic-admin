<div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">
    <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
        <div class="text-dark order-2 order-md-1">
            <b>Version</b> {{MetronicAdmin::version()}}
        </div>
        <div class="text-dark order-2 order-md-1 float-right">
            {!! config('metronic.footer.copy') !!}
        </div>
    </div>
</div>