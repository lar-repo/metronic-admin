    <!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light text-sm" @watch>
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item d-block d-sm-none">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-click="doc::back" title="@lang('metronic.back')" href="javascript:void(0)"><i class="fas fa-arrow-left"></i></a>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-click="doc::reload" title="@lang('metronic.refresh')" href="javascript:void(0)"><i class="fas fa-redo-alt"></i></a>
        </li>
        @if(admin()->isRoot())
            <li class="nav-item">
                <a class="nav-link" title="Root tools" data-toggle="collapse" href="#root-tools" role="button" aria-expanded="false" aria-controls="root-tools"><i class="fas fa-chess"></i></a>
            </li>
        @endif
{{--        <li class="nav-item d-none d-sm-inline-block">--}}
{{--            <a href="../../index3.html" class="nav-link">Home</a>--}}
{{--        </li>--}}
{{--        <li class="nav-item d-none d-sm-inline-block">--}}
{{--            <a href="#" class="nav-link">Contact</a>--}}
{{--        </li>--}}

        @foreach(gets()->metronic->menu->nested_collect->where('left_nav_bar_view') as $menu)
            @if(View::exists($menu['left_nav_bar_view']))
                @include($menu['left_nav_bar_view'], $menu['params'])
            @else
                {!! new $menu['left_nav_bar_view'](...$menu['params']); !!}
            @endif
        @endforeach
    </ul>

    <!-- SEARCH FORM -->
    {!! \Lar\MetronicAdmin\Components\Vue\GlobalSearch::create() !!}
    {!! \Lar\MetronicAdmin\Components\Vue\LiveReloader::create() !!}

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        @foreach(gets()->metronic->menu->nested_collect->where('badge')->where('link') as $menu)
            @php
                $counter = isset($menu['badge']['instructions']) && $menu['badge']['instructions'] ?
                    eloquent_instruction($menu['badge']['text'], $menu['badge']['instructions'])->count() :
                    $menu['badge']['text'];
                $link = isset($menu['link']) ? (isset($menu['badge']['params']) ? makeUrlWithParams($menu['link'], $menu['badge']['params']) : $menu['link']) : 'javascript:void(0)';
            @endphp
            @if($counter)
                <li class="nav-item">
                    <a class="nav-link" href="{{$link}}" title="{{__($menu['badge']['title'] ?? $menu['title'])}}">
                        <i class="{{$menu['icon']}}"></i>
                        <span class="badge badge-{{isset($menu['badge']['type']) ? $menu['badge']['type'] : 'info'}} navbar-badge">{{$counter}}</span>
                    </a>
                </li>
            @endif
        @endforeach

        @foreach(gets()->metronic->menu->nested_collect->where('nav_bar_view')->where('prepend', false) as $menu)
            @if(View::exists($menu['nav_bar_view']))
                @include($menu['nav_bar_view'], $menu['params'])
            @else
                {!! new $menu['nav_bar_view'](...$menu['params']); !!}
            @endif
        @endforeach
        <li>
            <a class="nav-link" target="_blank" href="{{url('/')}}" title="{{__('metronic.open_homepage_in_new_tab')}}"><i class="fas fa-external-link-square-alt"></i></a>
        </li>
        <li>
            <a class="nav-link" href="javascript:void(0)" data-click="alert::confirm" data-params="{{__('metronic.logout')}}, {{admin()->name}}? && {{route('metronic.profile.logout')}} >> $jax.get" title="{{__('metronic.logout')}}"><i class="fas fa-sign-out-alt"></i></a>
        </li>
        @if(config('layout.lang_mode'))
            <li class="nav-item dropdown language_dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="{{isset(config('metronic.lang_flags')[App::getLocale()]) ? config('metronic.lang_flags')[App::getLocale()] : ''}}"></i> {{strtoupper(App::getLocale())}}
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    @foreach(config('layout.languages') as $lang)
                        <a class="dropdown-item {{App::getLocale() == $lang ? 'active' : ''}}" href="{{remake_lang_url($lang)}}">
                            <i class="{{isset(config('metronic.lang_flags')[$lang]) ? config('metronic.lang_flags')[$lang] : ''}}"></i> {{strtoupper($lang)}}
                        </a>
                    @endforeach
                </div>
            </li>
        @endif

        @foreach(gets()->metronic->menu->nested_collect->where('nav_bar_view')->where('prepend', true) as $menu)
            @if(View::exists($menu['nav_bar_view']))
                @include($menu['nav_bar_view'], $menu['params'])
            @else
                {!! new $menu['nav_bar_view'](...$menu['params']); !!}
            @endif
        @endforeach
    </ul>
</nav>
<!-- /.navbar -->
