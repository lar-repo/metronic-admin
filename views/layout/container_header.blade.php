<!-- Content Header (Page header) -->
@php
    $menu = gets()->metronic->menu->now;
    $__head_title = ['MetronicAdmin'];
@endphp

<div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
    <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
        <div class="d-flex align-items-center flex-wrap mr-2">
            <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">
                @if(isset($page_info))
                    @if(is_array($page_info))
                        @if(isset($page_info['icon'])) <i class="{{$page_info['icon']}}"></i> @elseif(isset($menu['icon'])) <i class="{{$menu['icon']}}"></i>  @endif {!! __($page_info['head_title'] ?? ($page_info['title'] ?? ($menu['head_title'] ?? ($menu['title'] ?? 'Blank page')))) !!}
                    @else
                        @if(isset($menu['icon'])) <i class="{{$menu['icon']}}"></i>  @endif {{__($page_info)}}
                    @endif
                @else
                    @if(isset($menu['icon'])) <i class="{{$menu['icon']}}"></i> @endif {!! __($menu['head_title'] ?? ($menu['title'] ?? 'Blank page')) !!}
                @endif
            </h5>
        </div>

        <div class="d-flex align-items-center">
            @php($first = gets()->metronic->menu->nested_collect->first())

            @if (isset($breadcrumb) && is_array($breadcrumb) && count($breadcrumb))
                <ol class="breadcrumb float-sm-right m-0">
                    @foreach($breadcrumb as $item)
                        @if (is_array($item))
                            @foreach($item as $i)
                                <li class="breadcrumb-item {{$loop->last ? '' : 'active'}}">
                                    {!! __($i) !!}
                                    @php($__head_title[] = __($i))
                                </li>
                            @endforeach
                        @else
                            <li class="breadcrumb-item {{$loop->last ? '' : 'active'}}">
                                {!! __($item) !!}
                                @php($__head_title[] = __($item))
                            </li>
                        @endif
                    @endforeach
                </ol>
            @else
                @if (gets()->metronic->menu->now_parents->count() && $first['id'] !== $menu['id'])
                    <ol class="breadcrumb float-sm-right m-0">
                        <li class="breadcrumb-item active">
                            {!! __($first['title']) !!}
                        </li>
                        @foreach(gets()->metronic->menu->now_parents->reverse() as $item)
                            <li class="breadcrumb-item {{$loop->last ? '' : 'active'}}">
                                {!! __($item['title']) !!}
                                @php($__head_title[] = __($item['title']))
                            </li>
                        @endforeach
                    </ol>
                @endif
            @endif
        </div>

    </div>
</div>

@if (admin()->isRoot())
    {!! \Lar\MetronicAdmin\Components\RootTools::create() !!}
@endif