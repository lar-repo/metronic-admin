<?php

namespace Lar\MetronicAdmin\Tests\Unit;

use Tests\TestCase;

/**
 * Class HelpersTest
 * @package Lar\MetronicAdmin\Tests\Unit
 */
class HelpersTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testRelatedMethods()
    {
        $this->assertTrue(metronic_related_methods('store') === ['store', 'create', 'access']);
        $this->assertTrue(metronic_related_methods('update') === ['update', 'edit', 'access']);
        $this->assertTrue(metronic_related_methods('create') === ['create', 'store', 'access']);
        $this->assertTrue(metronic_related_methods('edit') === ['edit', 'update', 'access']);
        $this->assertTrue(metronic_related_methods('destroy') === ['destroy', 'delete', 'access']);
        $this->assertTrue(metronic_related_methods('delete') === ['delete', 'destroy', 'access']);
        $this->assertTrue(metronic_related_methods('test') === ['test', 'access']);
    }
}
