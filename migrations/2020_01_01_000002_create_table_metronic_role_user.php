<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMetronicRoleUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metronic_role_user', function (Blueprint $table) {

            $table->unsignedBigInteger('metronic_role_id');

            $table->unsignedBigInteger('metronic_user_id');

            $table->timestamps();

            $table->foreign('metronic_role_id')->references('id')->on('metronic_roles')->onDelete('cascade');

            $table->foreign('metronic_user_id')->references('id')->on('metronic_users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metronic_role_user');
    }
}
