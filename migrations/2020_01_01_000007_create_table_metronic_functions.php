<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMetronicFunctions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metronic_functions', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->string('slug')->unique();

            $table->text('description')->nullable();

            $table->boolean("active")->default(1);

            $table->timestamps();
        });

        Schema::create('metronic_role_function', function (Blueprint $table) {

            $table->unsignedBigInteger('metronic_role_id');

            $table->unsignedBigInteger('metronic_function_id');

            $table->foreign('metronic_role_id')->references('id')->on('metronic_roles')->onDelete('cascade');

            $table->foreign('metronic_function_id')->references('id')->on('metronic_functions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metronic_permission');
        Schema::dropIfExists('metronic_role_function');
    }
}
