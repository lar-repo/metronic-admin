<?php

namespace Lar\MetronicAdmin;

use Lar\Layout\Abstracts\Component;
use Lar\MetronicAdmin\Core\TableExtends\Decorations;
use Lar\MetronicAdmin\Core\TableExtends\Display;
use Lar\MetronicAdmin\Core\TableExtends\Editables;
use Lar\MetronicAdmin\Core\TableExtends\Formatter;
use Lar\MetronicAdmin\Core\TagableComponent;
use Lar\MetronicAdmin\Models\MetronicFunction;
use Lar\MetronicAdmin\Models\MetronicUser;
use Lar\MetronicAdmin\Segments\Tagable\Form;
use Lar\MetronicAdmin\Segments\Tagable\ModelTable;

/**
 * Class MetronicBoot
 * @package Lar\MetronicAdmin
 */
class MetronicBoot
{
    /**
     * Table extensions
     * @var array
     */
    protected static $table_classes = [
        Editables::class,
        Formatter::class,
        Decorations::class,
        Display::class
    ];
    
    /**
     * Run boot metronic scripts
     */
    public static function run()
    {
        /**
         * Register tagable components
         */
        TagableComponent::create();

        include __DIR__ . '/bootstrap.php';

        foreach (static::$table_classes as $item) {

            ModelTable::addExtensionClass($item);
        }

        foreach (\MetronicAdmin::extensions() as $extension) {

            if ($extension->included()) {

                $extension->config()->boot();
            }
        }

        static::formMacros();

        if (\Schema::hasTable('metronic_functions')) {

            static::makeGates();
        }
    }

    /**
     * Make gates for controller
     */
    protected static function makeGates()
    {
        /** @var MetronicFunction $item */
        foreach (MetronicFunction::with('roles')->where('active', 1)->get() as $item) {

            \Gate::define("{$item->class}@{$item->slug}", function (MetronicUser $user) use ($item) {
                return $user->hasRoles($item->roles->pluck('slug')->toArray());
            });
        }
    }

    /**
     * Make helper form
     */
    protected static function formMacros()
    {
        Form::macro('info_at', function ($condition = null) {
            if ($condition === null) $condition = gets()->metronic->menu->type === 'edit';
            if ($condition) $this->hr();
            $this->if($condition)->info('updated_at', 'metronic.updated_at');
            $this->if($condition)->info('created_at', 'metronic.created_at');
        });
        
        Form::macro('info_id', function ($condition = null) {
            if ($condition === null) $condition = gets()->metronic->menu->type === 'edit';
            $this->if($condition)->info('id', 'metronic.id');
        });
    }
}
