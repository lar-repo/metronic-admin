<?php

namespace Lar\MetronicAdmin\Core;

use Lar\Layout\Abstracts\Component;
use Lar\MetronicAdmin\Segments\Tagable\Alert;
use Lar\MetronicAdmin\Segments\Tagable\ButtonGroup;
use Lar\MetronicAdmin\Segments\Tagable\Card;
use Lar\MetronicAdmin\Segments\Tagable\Col;
use Lar\MetronicAdmin\Segments\Tagable\Divider;
use Lar\MetronicAdmin\Segments\Tagable\Field;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Hidden;
use Lar\MetronicAdmin\Segments\Tagable\Form;
use Lar\MetronicAdmin\Segments\Tagable\FormFooter;
use Lar\MetronicAdmin\Segments\Tagable\InfoBox;
use Lar\MetronicAdmin\Segments\Tagable\ModelInfoTable;
use Lar\MetronicAdmin\Segments\Tagable\ModelRelation;
use Lar\MetronicAdmin\Segments\Tagable\ModelTable;
use Lar\MetronicAdmin\Segments\Tagable\Nested;
use Lar\MetronicAdmin\Segments\Tagable\Row;
use Lar\MetronicAdmin\Segments\Tagable\SmallBox;
use Lar\MetronicAdmin\Segments\Tagable\Table;
use Lar\MetronicAdmin\Segments\Tagable\Tabs;

/**
 * Class TagableComponent
 * @package Lar\MetronicAdmin\Core
 */
class TagableComponent extends Component {

    /**
     * @var string[]
     */
    protected $collection = [
        'row' => Row::class,
        'col' => Col::class,
        'card' => Card::class,
        'form' => Form::class,
        'form_footer' => FormFooter::class,
        'field' => Field::class,
        'model_table' => ModelTable::class,
        'model_info_table' => ModelInfoTable::class,
        'table' => Table::class,
        'button_group' => ButtonGroup::class,
        'alert' => Alert::class,
        'small_box' => SmallBox::class,
        'info_box' => InfoBox::class,
        'tabs' => Tabs::class,
        'nested' => Nested::class,
        'divider' => Divider::class,
        'model_relation' => ModelRelation::class
    ];

    /**
     * TagableComponent constructor.
     */
    public function __construct()
    {
        static::injectCollection($this->collection);
    }
}