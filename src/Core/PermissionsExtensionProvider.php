<?php

namespace Lar\MetronicAdmin\Core;

use Illuminate\Console\Command;
use Lar\MetronicAdmin\ExtendProvider;
use Lar\MetronicAdmin\Models\MetronicFunction;
use Lar\MetronicAdmin\Models\MetronicRole;

/**
 * Class InstallExtensionProvider
 * @package Lar\MetronicAdmin\Core
 */
class PermissionsExtensionProvider {

    /**
     * @var Command
     */
    public $command;
    /**
     * @var ExtendProvider
     */
    public $provider;

    /**
     * InstallExtensionProvider constructor.
     * @param  Command  $command
     * @param  ExtendProvider  $provider
     */
    public function __construct(Command $command, ExtendProvider $provider)
    {
        $this->command = $command;
        $this->provider = $provider;
    }

    /**
     * Make all extension permissions
     */
    public function up()
    {
        if (method_exists($this, 'roles')) {
            $roles = $this->roles();
            if (is_array($roles)) {
                ModelSaver::doMany(MetronicRole::class, $roles);
                if (count($roles)) { $this->command->info('Created ' . count($roles) . ' roles.'); }
            }
        }
        $pushed = ModelSaver::doMany(MetronicFunction::class, array_merge([$this->makeFunction('access')], $this->functions()));
        if ($pushed->count()) { $this->command->info('Created ' . $pushed->count() . ' permission functions.'); }
    }

    /**
     * Drop all extension permissions
     * @throws \Exception
     */
    public function down()
    {
        if (method_exists($this, 'roles')) {
            $roles = $this->roles();
            if (is_array($roles)) {
                $roles_count = 0;
                foreach ($roles as $role) {
                    if (MetronicRole::where('slug', $role['slug'])->delete()) {$roles_count++;}
                }
                if ($roles_count) {
                    $this->command->info('Deleted '.$roles_count.' roles.');
                }
            }
        }

        $functions_count = 0;
        foreach (array_merge([$this->makeFunction('access')], $this->functions()) as $function) {
            $del = isset($function['class']) ?
                MetronicFunction::where('slug', $function['slug'])->where('class', $function['class'])->delete() :
                MetronicFunction::where('slug', $function['slug'])->delete();

            if (is_array($function) && count($function) && $del) {
                $functions_count++;
            }
        }
        if ($functions_count) { $this->command->info('Deleted ' . $functions_count . ' permission functions.'); }
    }

    /**
     * @param  array  $roles
     * @param  string|null  $slug
     * @param  string|null  $description
     * @return array
     */
    public function makeFunction(string $slug, array $roles = ['*'], string $description = null): array
    {
        return [
            'slug' => $slug,
            'class' => get_class($this->provider),
            'description' => $this->provider::$description . ($description ? " [$description]" : (\Lang::has("metronic.about_method.{$slug}") ? " [@metronic.about_method.{$slug}]":" [{$slug}]")),
            'roles' => $roles === ['*'] ? MetronicRole::all()->pluck('id')->toArray() : collect($roles)->map(function ($item) {
                return is_numeric($item) ? $item : MetronicRole::where('slug', $item)->first()->id;
            })->filter()->values()->toArray()
        ];
    }

    /**
     * @return array
     */
    public function functions(): array {

        return [];
    }
}