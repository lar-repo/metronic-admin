<?php

namespace Lar\MetronicAdmin\Core;

use Illuminate\Contracts\Support\Arrayable;
use Lar\Layout\Traits\FontAwesome;
use Lar\MetronicAdmin\Core\Traits\NavCommon;
use Lar\MetronicAdmin\Core\Traits\NavDefaultTools;
use Lar\MetronicAdmin\Interfaces\NavigateInterface;
use Lar\MetronicAdmin\MetronicAdmin;
use Lar\MetronicAdmin\MetronicNavigate;

/**
 * Class NavGroup
 * @package Lar\MetronicAdmin\Core
 * @mixin NavigatorExtensions
 */
class NavGroup implements Arrayable, NavigateInterface
{
    use FontAwesome, NavCommon, NavDefaultTools;
    
    /**
     * @var array
     */
    public $items = [];

    /**
     * NavItem constructor.
     * @param  string|null  $title
     * @param  string  $route
     */
    public function __construct(string $title = null, string $route = null)
    {
        $this->title($title)
            ->route($route)
            ->extension(MetronicNavigate::$extension);

    }

    /**
     * @param  \Closure|array  ...$calls
     * @return $this
     */
    public function do(...$calls)
    {
        foreach ($calls as $call) {

            if (is_embedded_call($call)) {

                call_user_func($call, $this);
            }
        }

        return $this;
    }

    /**
     * @param  string|null  $title
     * @param  string|null  $route
     * @param  string|\Closure|null  $action
     * @return NavItem
     */
    public function item(string $title = null, string $route = null, $action = null)
    {
        $item = new NavItem($title, $route, $action);

        $this->items['items'][] = $item;

        if (isset($item->items['route'])) {

            $this->includeAfterGroup($item->items['route']);
        }

        return $item;
    }

    /**
     * @param  string|null  $title
     * @param  string|null|\Closure|array  $route
     * @param  \Closure|array|null  $cb
     * @return NavGroup
     */
    public function group(string $title = null, $route = null, $cb = null)
    {
        if (is_embedded_call($route) && !is_string($route)) {
            $cb = $route;
            $route = null;
        }

        $item = new NavGroup($title, $route);

        $this->items['items'][] = $item;

        if (isset($item->items['route'])) {

            $this->includeAfterGroup($item->items['route']);
        }

        if (is_embedded_call($cb)) {

            call_user_func($cb, $item);
        }

        return $item;
    }

    /**
     * @param  string  $view
     * @param  array  $params
     * @param  bool  $prepend
     * @return $this
     */
    public function nav_bar_view(string $view, array $params = [], bool $prepend = false)
    {
        MetronicNavigate::$items[] = collect(['nav_bar_view' => $view, 'params' => $params, 'prepend' => $prepend]);

        return $this;
    }

    /**
     * @param  string  $view
     * @param  array  $params
     * @return $this
     */
    public function left_nav_bar_view(string $view, array $params = [])
    {
        MetronicNavigate::$items[] = collect(['left_nav_bar_view' => $view, 'params' => $params]);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function toArray()
    {
        if (isset($this->items['items'])) {
            foreach ($this->items['items'] as $key => $item) {
                /** @var Arrayable $item */
                $this->items['items'][$key] = $item->toArray();
            }
        }

        return $this->items;
    }

    /**
     * @param $name
     * @param $arguments
     */
    public function __call($name, $arguments)
    {
        if (isset(MetronicAdmin::$nav_extensions[$name])) {

            MetronicNavigate::$extension = MetronicAdmin::$nav_extensions[$name];

            MetronicAdmin::$nav_extensions[$name]->navigator($this);

            MetronicNavigate::$extension = null;

            unset(MetronicAdmin::$nav_extensions[$name]);
        }
    }
}