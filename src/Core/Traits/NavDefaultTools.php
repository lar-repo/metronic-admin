<?php

namespace Lar\MetronicAdmin\Core\Traits;

use Lar\MetronicAdmin\Core\NavGroup;

/**
 * Trait NavDefaultToos
 * @package Lar\MetronicAdmin\Core\Traits
 */
trait NavDefaultTools
{
    /**
     * Make auto default tools
     * @return $this
     */
    public function makeDefaults()
    {
        $this->metronicAdministrationGroup(function  (NavGroup $group) {

            $group->metronicAdministrators();
            $group->metronicRoles();
            $group->metronicPermission();
            $group->metronicFunctions();

//            $group->metronicAccessGroup(function (NavGroup $group) {
//
//                $group->metronicRoles();
//                $group->metronicPermission();
//                $group->metronicFunctions();
//            });
        });

        return $this;
    }

    /**
     * Make default administration group
     * @param  \Closure|array  $call
     * @return \Lar\MetronicAdmin\Core\NavGroup
     */
    public function metronicAdministrationGroup($call)
    {
        return $this->group('metronic.administration', 'admin', function (NavGroup $group) use ($call) {

            if (is_embedded_call($call)) {

                call_user_func($call, $group);
            }

        })->icon_cogs();
    }

    /**
     * Make administrator list tool
     * @param  string|null  $action
     * @return \Lar\MetronicAdmin\Core\NavItem
     */
    public function metronicAdministrators(string $action = null)
    {
        return $this->item('metronic.administrators', 'administrators')
            ->resource('metronic_user', $action ?? '\Lar\MetronicAdmin\Controllers\AdministratorsController')
            ->icon_users_cog();
    }

    /**
     * Make default access group
     * @param  \Closure|array  $call
     * @return \Lar\MetronicAdmin\Core\NavGroup
     */
    public function metronicAccessGroup($call)
    {
        return $this->group('metronic.access', 'access', function (NavGroup $group) use ($call) {

            if (is_embedded_call($call)) {

                call_user_func($call, $group);
            }

        })->icon_universal_access();
    }

    /**
     * Make roles list tool
     * @param  string|null  $action
     * @return \Lar\MetronicAdmin\Core\NavItem
     */
    public function metronicRoles(string $action = null)
    {
        return $this->item('metronic.roles', 'roles')
            ->resource('metronic_role', $action ?? '\Lar\MetronicAdmin\Controllers\RolesController')
            ->icon_user_secret();
    }

    /**
     * Make permissions list tool
     * @param  string|null  $action
     * @return \Lar\MetronicAdmin\Core\NavItem
     */
    public function metronicPermission(string $action = null)
    {
        return $this->item('metronic.permission', 'permission')
            ->resource('metronic_permission', $action ?? '\Lar\MetronicAdmin\Controllers\PermissionController')
            ->icon_ban();
    }

    /**
     * Make functions/gates list tool
     * @param  string|null  $action
     * @return \Lar\MetronicAdmin\Core\NavItem
     */
    public function metronicFunctions(string $action = null)
    {
        return $this->item('metronic.functions', 'functions')
            ->resource('metronic_functions', $action ?? '\Lar\MetronicAdmin\Controllers\FunctionsController')
            ->icon_dungeon()->role('root');
    }
}