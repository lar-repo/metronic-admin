<?php

namespace Lar\MetronicAdmin\Core\Generators;

use Illuminate\Console\Command;
use Lar\Developer\Commands\Dump\DumpExecute;
use Lar\EntityCarrier\Core\Entities\ClassEntity;
use Lar\EntityCarrier\Core\Entities\DocumentorEntity;
use Lar\MetronicAdmin\Core\NavGroup;
use Lar\MetronicAdmin\Models\MetronicFunction;
use Lar\MetronicAdmin\MetronicNavigate;

/**
 * Class FunctionsHelperGenerator
 * @package Lar\MetronicAdmin\Core
 */
class ExtensionNavigatorHelperGenerator implements DumpExecute {

    /**
     * @param  Command  $command
     * @return mixed|string
     */
    public function handle(Command $command)
    {
        $namespace = namespace_entity("Lar\MetronicAdmin\Core");

        $namespace->class("NavigatorExtensions", function (ClassEntity $class) {

            $class->doc(function (DocumentorEntity $doc) {

                $this->generateDefaultMethods($doc);
            });
        });

        $namespace->class("NavigatorMethods", function (ClassEntity $class) {

            $class->doc(function (DocumentorEntity $doc) {

                $this->generateAllMethods($doc);
            });
        });

        return $namespace->render();
    }

    /**
     * Generate all methods
     *
     * @param  DocumentorEntity  $doc
     */
    protected function generateAllMethods(DocumentorEntity $doc)
    {
        $methods = [];

        $nav = new \ReflectionClass(MetronicNavigate::class);

        foreach ($nav->getMethods() as $method) {
            $methods[$method->getName()] = $method;
        }
        foreach ($methods as $method) {

            $ret = pars_return_from_doc($method->getDocComment());

            $doc->tagMethod('self|static|' . ($ret ? $ret : '\\'.MetronicNavigate::class), $method->getName() . "(".refl_params_entity($method->getParameters()).")", pars_description_from_doc($method->getDocComment()));
        }
    }

    /**
     * Generate default methods
     *
     * @param DocumentorEntity $doc
     * @throws \ReflectionException
     */
    protected function generateDefaultMethods(DocumentorEntity $doc)
    {
        foreach (\MetronicAdmin::extensions() as $name => $provider) {

            $doc->tagMethod('void', $provider::$slug, "Make extension routes ($name})");
        }
    }
}