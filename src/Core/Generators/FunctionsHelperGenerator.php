<?php

namespace Lar\MetronicAdmin\Core\Generators;

use Illuminate\Console\Command;
use Lar\Developer\Commands\Dump\DumpExecute;
use Lar\EntityCarrier\Core\Entities\ClassEntity;
use Lar\EntityCarrier\Core\Entities\DocumentorEntity;
use Lar\MetronicAdmin\Models\MetronicFunction;

/**
 * Class FunctionsHelperGenerator
 * @package Lar\MetronicAdmin\Core
 */
class FunctionsHelperGenerator implements DumpExecute {

    /**
     * @param  Command  $command
     * @return mixed|string
     */
    public function handle(Command $command)
    {
        $namespace = namespace_entity("Lar\MetronicAdmin\Core");

        $namespace->wrap('php');

        $namespace->class("FunctionsDoc", function (ClassEntity $class) {

            $class->doc(function (DocumentorEntity $doc) {

                $this->generateDefaultMethods($doc);
            });
        });

        file_put_contents(base_path('_ide_helper_metronic_func.php'), $namespace->render());
    }

    /**
     * Generate default methods
     *
     * @param DocumentorEntity $doc
     * @throws \ReflectionException
     */
    protected function generateDefaultMethods(DocumentorEntity $doc)
    {
        foreach (MetronicFunction::all() as $func) {

            $doc->tagPropertyRead('bool', $func->slug, "Check if the user has access to the function ({$func->description})");
        }
    }
}