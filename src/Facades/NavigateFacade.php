<?php

namespace Lar\MetronicAdmin\Facades;

use Illuminate\Support\Facades\Facade as FacadeIlluminate;
use Lar\MetronicAdmin\MetronicNavigate;

/**
 * Class Facade
 * 
 * @package Lar
 */
class NavigateFacade extends FacadeIlluminate
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return MetronicNavigate::class;
    }
}
