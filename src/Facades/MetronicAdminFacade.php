<?php

namespace Lar\MetronicAdmin\Facades;

use Illuminate\Support\Facades\Facade as FacadeIlluminate;
use Lar\MetronicAdmin\MetronicAdmin;

/**
 * Class Facade
 * 
 * @package Lar
 */
class MetronicAdminFacade extends FacadeIlluminate
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return MetronicAdmin::class;
    }
}
