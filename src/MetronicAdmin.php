<?php

namespace Lar\MetronicAdmin;

use Lar\MetronicAdmin\Components\Vue\ModalCollection;
use Lar\MetronicAdmin\Components\Vue\Navigator;

/**
 * Class MetronicAdmin
 *
 * @package Lar\CryptoApi
 */
class MetronicAdmin
{
    /**
     * @var string
     */
    static $version = "1.0.0";

    /**
     * @var ExtendProvider[]
     */
    static $nav_extensions = [];

    /**
     * @var ExtendProvider[]
     */
    static $installed_extensions = [];

    /**
     * @var ExtendProvider[]
     */
    static $not_installed_extensions = [];

    /**
     * @var bool[]
     */
    static $extensions;

    /**
     * @var bool
     */
    static $echo = false;

    /**
     * @var array
     */
    protected $content_segments = [
        'app_end_wrapper' => [],
        'prep_end_wrapper' => [
            //['component' => Navigator::class, 'params' => []],
            ['component' => ModalCollection::class, 'params' => []]
        ],
        'app_end_content' => [],
        'prep_end_content' => [],
    ];

    /**
     * @return \Lar\MetronicAdmin\Models\MetronicUser|\Illuminate\Contracts\Auth\Authenticatable|\App\Models\Admin
     */
    public function user()
    {
        return \Auth::guard('metronic')->user();
    }

    /**
     * @return mixed
     */
    public function guest()
    {
        return \Auth::guard('metronic')->guest();
    }

    /**
     * @return string
     */
    public function version()
    {
        return MetronicAdmin::$version;
    }

    /**
     * @param  string  $segment
     * @param  string  $component
     * @param  array  $params
     * @return $this
     */
    public function toSegment(string $segment, string $component, array $params = [])
    {
        if (isset($this->content_segments[$segment])) {

            $this->content_segments[$segment][] = ['component' => $component, 'params' => $params];
        }

        return $this;
    }

    /**
     * @param  string  $component
     * @param  array  $params
     * @param  bool  $prepend
     * @return $this
     */
    public function toWrapper(string $component, array $params = [], bool $prepend = false)
    {
        $segment = $prepend ? "prep_end_wrapper" : "app_end_wrapper";

        return $this->toSegment($segment, $component, $params);
    }

    /**
     * @param  string  $component
     * @param  array  $params
     * @param  bool  $prepend
     * @return $this
     */
    public function toContent(string $component, array $params = [], bool $prepend = false)
    {
        $segment = $prepend ? "prep_end_content" : "app_end_content";

        return $this->toSegment($segment, $component, $params);
    }

    /**
     * @param  string  $segment
     * @return array
     */
    public function getSegments(string $segment)
    {
        if (isset($this->content_segments[$segment])) {

            return $this->content_segments[$segment];
        }

        return [];
    }

    /**
     * @param  ExtendProvider  $provider
     * @throws \Exception
     */
    public function registerExtension(ExtendProvider $provider)
    {
        if (!$provider::$name) {

            return false;
        }

        if (!MetronicAdmin::$extensions) {

            MetronicAdmin::$extensions = include storage_path('metronic_extensions.php');
        }

        if (isset(MetronicAdmin::$extensions[$provider::$name]) || $provider::$slug === 'application') {

            if (!isset(MetronicAdmin::$installed_extensions[$provider::$name])) {

                MetronicAdmin::$installed_extensions[$provider::$name] = $provider;

                if ($provider->included()) {

                    if (!$provider::$after) {
                        MetronicAdmin::$nav_extensions[$provider::$slug] = $provider;
                    } else {
                        MetronicAdmin::$nav_extensions[$provider::$after][] = $provider;
                    }
                }
            }
        }

        else if (!isset(MetronicAdmin::$not_installed_extensions[$provider::$name])) {

            MetronicAdmin::$not_installed_extensions[$provider::$name] = $provider;
        }

        return true;
    }

    /**
     * @param  string  $name
     * @return bool|ExtendProvider
     */
    public function extension(string $name)
    {
        if (isset(MetronicAdmin::$installed_extensions[$name])) {

            return MetronicAdmin::$installed_extensions[$name];
        }

        return false;
    }

    /**
     * @return ExtendProvider[]
     */
    public function extensions()
    {
        return MetronicAdmin::$installed_extensions;
    }

    /**
     * @param  string  $name
     * @return ExtendProvider|null
     */
    public function getExtension(string $name)
    {
        if (isset(MetronicAdmin::$installed_extensions[$name])) {
            return MetronicAdmin::$installed_extensions[$name];
        } else if (MetronicAdmin::$not_installed_extensions[$name]) {
            return MetronicAdmin::$not_installed_extensions[$name];
        }
        return null;
    }

    /**
     * @return string[]
     */
    public function extensionProviders()
    {
        return array_flip(
            array_map(
                'get_class',
                array_merge(
                    MetronicAdmin::$installed_extensions,
                    MetronicAdmin::$not_installed_extensions
                )
            )
        );
    }
}
