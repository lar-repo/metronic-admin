<?php

namespace Lar\MetronicAdmin\Segments\Tagable;

use Lar\Layout\Tags\DIV;
use Lar\MetronicAdmin\Core\Traits\Macroable;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Amount;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Checks;
use Lar\MetronicAdmin\Segments\Tagable\Fields\CKEditor;
use Lar\MetronicAdmin\Segments\Tagable\Fields\CodeMirror;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Color;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Date;
use Lar\MetronicAdmin\Segments\Tagable\Fields\DateRange;
use Lar\MetronicAdmin\Segments\Tagable\Fields\DateTime;
use Lar\MetronicAdmin\Segments\Tagable\Fields\DateTimeRange;
use Lar\MetronicAdmin\Segments\Tagable\Fields\DualSelect;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Email;
use Lar\MetronicAdmin\Segments\Tagable\Fields\File;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Hidden;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Icon;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Image;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Info;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Input;
use Lar\MetronicAdmin\Segments\Tagable\Fields\MDEditor;
use Lar\MetronicAdmin\Segments\Tagable\Fields\MultiSelect;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Number;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Numeric;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Password;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Radios;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Rating;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Select;
use Lar\MetronicAdmin\Segments\Tagable\Fields\SelectTags;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Switcher;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Textarea;
use Lar\MetronicAdmin\Segments\Tagable\Fields\Time;
use Lar\MetronicAdmin\Segments\Tagable\Traits\FieldMassControl;
use Lar\Tagable\Events\onRender;

/**
 * Class Col
 * @package Lar\MetronicAdmin\Segments\Tagable
 * @macro_return Lar\MetronicAdmin\Segments\Tagable\FormGroup
 * @methods static::$form_components (string $name, string $label = null, ...$params)
 * @mixin FieldMethods
 * @mixin FieldMacroList
 */
class Field extends DIV implements onRender {

    use FieldMassControl, Macroable;

    /**
     * @var array
     */
    static $form_components = [
        'input' => Input::class,
        'password' => Password::class,
        'email' => Email::class,
        'number' => Number::class,
        'numeric' => Numeric::class,
        'amount' => Amount::class,
        'file' => File::class,
        'image' => Image::class,
        'switcher' => Switcher::class,
        'date_range' => DateRange::class,
        'date_time_range' => DateTimeRange::class,
        'date' => Date::class,
        'date_time' => DateTime::class,
        'time' => Time::class,
        'icon' => Icon::class,
        'color' => Color::class,
        'select' => Select::class,
        'dual_select' => DualSelect::class,
        'multi_select' => MultiSelect::class,
        'select_tags' => SelectTags::class,
        'textarea' => Textarea::class,
        'ckeditor' => CKEditor::class,
        'mdeditor' => MDEditor::class,
        'checks' => Checks::class,
        'radios' => Radios::class,
        'codemirror' => CodeMirror::class,
        'info' => Info::class,
        'rating' => Rating::class,
        'hidden' => Hidden::class,
    ];

    /**
     * @var bool
     */
    protected $only_content = true;

    /**
     * Fields constructor.
     * @param  mixed  ...$params
     */
    public function __construct(...$params)
    {
        parent::__construct();

        $this->when($params);

        $this->callConstructEvents();
    }

    /**
     * @param $name
     * @param $arguments
     * @return bool|Form|\Lar\Tagable\Tag|mixed|string
     * @throws \Exception
     */
    public function __call($name, $arguments)
    {
        if ($call = $this->call_group($name, $arguments)) {

            return $call;
        }

        return parent::__call($name, $arguments);
    }

    /**
     * @param $name
     * @param $arguments
     * @return bool|Field|FormGroup|mixed
     * @throws \Exception
     */
    public static function __callStatic($name, $arguments)
    {
        if ($call = static::static_call_group($name, $arguments)) {

            return $call;
        }

        return parent::__callStatic($name, $arguments);
    }

    /**
     * @param  string  $name
     * @param  string  $class
     */
    public static function registerFormComponent(string $name, string $class)
    {
        static::$form_components[$name] = $class;
    }

    /**
     * @param  array  $array
     */
    public static function mergeFormComponents(array $array)
    {
        static::$form_components = array_merge(static::$form_components, $array);
    }

    /**
     * @param  string  $name
     * @return bool
     */
    public static function has(string $name)
    {
        return isset(static::$form_components[$name]);
    }

    /**
     * @return mixed|void
     * @throws \ReflectionException
     */
    public function onRender()
    {
        $this->callRenderEvents();
    }
}