<?php

namespace Lar\MetronicAdmin\Segments\Tagable\SearchFields;

/**
 * Class Numeric
 * @package Lar\MetronicAdmin\Segments\Tagable\SearchFields
 */
class Numeric extends \Lar\MetronicAdmin\Segments\Tagable\Fields\Numeric
{
    /**
     * @var string
     */
    static $condition = "=";
}