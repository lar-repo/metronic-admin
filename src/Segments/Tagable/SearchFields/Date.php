<?php

namespace Lar\MetronicAdmin\Segments\Tagable\SearchFields;

use Carbon\Carbon;

/**
 * Class Date
 * @package Lar\MetronicAdmin\Segments\Tagable\Fields
 */
class Date extends \Lar\MetronicAdmin\Segments\Tagable\Fields\Date
{
    /**
     * @var string
     */
    static $condition = ">=";

    /**
     * @param $value
     * @return Carbon
     */
    static function transformValue ($value) {

        return Carbon::create($value);
    }
}