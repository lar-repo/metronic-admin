<?php

namespace Lar\MetronicAdmin\Segments\Tagable\SearchFields;

/**
 * Class MultiSelect
 * @package Lar\MetronicAdmin\Segments\Tagable\SearchFields
 */
class MultiSelect extends \Lar\MetronicAdmin\Segments\Tagable\Fields\MultiSelect
{
    /**
     * @var string
     */
    static $condition = "in";
}