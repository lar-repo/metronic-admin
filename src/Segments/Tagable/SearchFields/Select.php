<?php

namespace Lar\MetronicAdmin\Segments\Tagable\SearchFields;

/**
 * Class Select
 * @package Lar\MetronicAdmin\Segments\Tagable\SearchFields
 */
class Select extends \Lar\MetronicAdmin\Segments\Tagable\Fields\Select
{
    /**
     * @var string
     */
    static $condition = "=";

    /**
     * After construct event
     */
    protected function after_construct()
    {
        $this->nullable();
    }
}