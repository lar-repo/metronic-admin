<?php

namespace Lar\MetronicAdmin\Segments\Tagable\SearchFields;

/**
 * Class Switcher
 * @package Lar\MetronicAdmin\Segments\Tagable\SearchFields
 */
class Switcher extends \Lar\MetronicAdmin\Segments\Tagable\Fields\Switcher
{
    /**
     * @var string
     */
    static $condition = "=";
}