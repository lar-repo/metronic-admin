<?php

namespace Lar\MetronicAdmin\Segments\Tagable\SearchFields;

use Carbon\Carbon;

/**
 * Class DateTime
 * @package Lar\MetronicAdmin\Segments\Tagable\Fields
 */
class DateTime extends \Lar\MetronicAdmin\Segments\Tagable\Fields\DateTime
{
    /**
     * @var string
     */
    static $condition = ">=";

    /**
     * @param $value
     * @return Carbon
     */
    static function transformValue ($value) {

        return Carbon::create($value);
    }
}