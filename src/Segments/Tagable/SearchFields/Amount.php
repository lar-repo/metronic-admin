<?php

namespace Lar\MetronicAdmin\Segments\Tagable\SearchFields;

/**
 * Class Amount
 * @package Lar\MetronicAdmin\Segments\Tagable\SearchFields
 */
class Amount extends \Lar\MetronicAdmin\Segments\Tagable\Fields\Amount
{
    /**
     * @var string
     */
    static $condition = ">=";
}