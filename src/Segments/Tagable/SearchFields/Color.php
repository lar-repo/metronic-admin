<?php

namespace Lar\MetronicAdmin\Segments\Tagable\SearchFields;

/**
 * Class Color
 * @package Lar\MetronicAdmin\Segments\Tagable\Fields
 */
class Color extends \Lar\MetronicAdmin\Segments\Tagable\Fields\Color
{
    /**
     * @var string
     */
    static $condition = "=";
}