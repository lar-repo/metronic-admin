<?php

namespace Lar\MetronicAdmin\Segments\Tagable\SearchFields;

/**
 * Class Checks
 * @package Lar\MetronicAdmin\Segments\Tagable\SearchFields
 */
class Checks extends \Lar\MetronicAdmin\Segments\Tagable\Fields\Checks
{
    /**
     * @var string
     */
    static $condition = "in";
}