<?php

namespace Lar\MetronicAdmin\Segments\Tagable\SearchFields;

/**
 * Class Radios
 * @package Lar\MetronicAdmin\Segments\Tagable\SearchFields
 */
class Radios extends \Lar\MetronicAdmin\Segments\Tagable\Fields\Radios
{
    /**
     * @var string
     */
    static $condition = "=";
}