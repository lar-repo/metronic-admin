<?php

namespace Lar\MetronicAdmin\Segments\Tagable\SearchFields;

/**
 * Class Number
 * @package Lar\MetronicAdmin\Segments\Tagable\SearchFields
 */
class Number extends \Lar\MetronicAdmin\Segments\Tagable\Fields\Number
{
    /**
     * @var string
     */
    static $condition = "=";
}