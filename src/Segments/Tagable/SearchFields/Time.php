<?php

namespace Lar\MetronicAdmin\Segments\Tagable\SearchFields;

/**
 * Class Time
 * @package Lar\MetronicAdmin\Segments\Tagable\SearchFields
 */
class Time extends \Lar\MetronicAdmin\Segments\Tagable\Fields\Time
{
    /**
     * @var string
     */
    static $condition = "=";
}