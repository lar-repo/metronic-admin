<?php

namespace Lar\MetronicAdmin\Segments\Tagable\SearchFields;

/**
 * Class SelectTags
 * @package Lar\MetronicAdmin\Segments\Tagable\SearchFields
 */
class SelectTags extends \Lar\MetronicAdmin\Segments\Tagable\Fields\SelectTags
{
    /**
     * @var string
     */
    static $condition = "in";

    /**
     * After construct event
     */
    protected function after_construct()
    {
        $this->nullable();
    }
}