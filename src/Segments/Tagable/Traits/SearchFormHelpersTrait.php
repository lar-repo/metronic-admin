<?php

namespace Lar\MetronicAdmin\Segments\Tagable\Traits;

/**
 * Trait SearchFormHelpersTrait
 * @package Lar\MetronicAdmin\Segments\Tagable\Traits
 */
trait SearchFormHelpersTrait {

    /**
     * @return $this
     */
    public function id()
    {
        $this->numeric('id', 'metronic.id', '=');

        return $this;
    }

    /**
     * @return $this
     */
    public function created_at()
    {
        $this->date_time_range('created_at', 'metronic.created_at');

        return $this;
    }

    /**
     * @return $this
     */
    public function updated_at()
    {
        $this->date_time_range('updated_at', 'metronic.updated_at');

        return $this;
    }

    /**
     * @return $this
     */
    public function at()
    {
        return $this->updated_at()->created_at();
    }
}