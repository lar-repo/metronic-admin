<?php

namespace Lar\MetronicAdmin\Segments\Tagable\Fields;

use Lar\MetronicAdmin\Segments\Tagable\FormGroup;

/**
 * Class MDEditor
 * @package Lar\MetronicAdmin\Segments\Tagable\Fields
 */
class MDEditor extends FormGroup
{
    /**
     * @var null
     */
    protected $icon = null;

    /**
     * @var string[]
     */
    protected $data = [
        'load' => 'md::simple'
    ];

    /**
     * @return \Lar\Layout\Abstracts\Component|\Lar\Layout\Tags\INPUT|mixed
     */
    public function field()
    {
        return \Lar\Layout\Tags\DIV::create([
            'id' => $this->field_id,
            'data-name' => $this->name,
            'data-placeholder' => $this->title,
            'm-0'
        ], ...$this->params)
            ->text(e($this->value))
            ->setRules($this->rules)
            ->setDatas($this->data)
            ->addClassIf($this->has_bug, 'is-invalid');
    }
}