<?php

namespace Lar\MetronicAdmin\Segments\Tagable\Fields;

use Lar\MetronicAdmin\Segments\Tagable\FormGroup;

/**
 * Class Hidden
 * @package Lar\MetronicAdmin\Segments\Tagable\Fields
 */
class Hidden extends FormGroup
{
    /**
     * @var string
     */
    protected $type = "hidden";

    /**
     * @var bool
     */
    protected $vertical = true;

    /**
     * @var null
     */
    protected $icon = null;

    /**
     * @return \Lar\Layout\Abstracts\Component|\Lar\Layout\Tags\INPUT|mixed
     */
    public function field()
    {
        return \Lar\Layout\Tags\INPUT::create([
            'type' => $this->type,
            'id' => $this->field_id,
            'name' => $this->name,
            'placeholder' => $this->title
        ], ...$this->params)
            ->setValue($this->value);
    }

    /**
     * @return $this
     */
    public function disabled()
    {
        $this->params[] = ['disabled' => 'true'];

        return $this;
    }
}