<?php

namespace Lar\MetronicAdmin\Segments\Tagable\Fields;

use Lar\MetronicAdmin\Segments\Tagable\Traits\DateControlTrait;

/**
 * Class DateTimeRange
 * @package Lar\MetronicAdmin\Segments\Tagable\Fields
 */
class DateTimeRange extends DateRange
{
    use DateControlTrait;

    /**
     * @var string[]
     */
    protected $data = [
        'load' => 'picker::datetimerange'
    ];
}