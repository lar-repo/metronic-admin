<?php

namespace Lar\MetronicAdmin\Segments\Tagable\Fields;

use Lar\MetronicAdmin\Segments\Tagable\Traits\DateControlTrait;

/**
 * Class DateRange
 * @package Lar\MetronicAdmin\Segments\Tagable\Fields
 */
class DateRange extends Input
{
    use DateControlTrait;

    /**
     * @var string
     */
    protected $icon = "fas fa-calendar";

    /**
     * @var string[]
     */
    protected $data = [
        'load' => 'picker::daterange'
    ];

    /**
     * @var array
     */
    protected $params = [
        ['autocomplete' => 'off']
    ];
}