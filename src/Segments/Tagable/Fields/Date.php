<?php

namespace Lar\MetronicAdmin\Segments\Tagable\Fields;

use Lar\MetronicAdmin\Segments\Tagable\Traits\DateControlTrait;

/**
 * Class Date
 * @package Lar\MetronicAdmin\Segments\Tagable\Fields
 */
class Date extends Input
{
    use DateControlTrait;

    /**
     * @var string
     */
    protected $icon = "fas fa-calendar-plus";

    /**
     * @var string[]
     */
    protected $data = [
        'load' => 'picker::date',
        'toggle' => 'datetimepicker'
    ];

    /**
     * @var string
     */
    protected $type = 'date';

    /**
     * On build
     */
    protected function on_build()
    {
        $this->data['target'] = "#{$this->field_id}";
    }

    /**
     * @var array
     */
    protected $params = [
        ['autocomplete' => 'off']
    ];
}