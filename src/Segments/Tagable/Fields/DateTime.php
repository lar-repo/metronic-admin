<?php

namespace Lar\MetronicAdmin\Segments\Tagable\Fields;

use Lar\MetronicAdmin\Segments\Tagable\Traits\DateControlTrait;

/**
 * Class DateTime
 * @package Lar\MetronicAdmin\Segments\Tagable\Fields
 */
class DateTime extends Input
{
    use DateControlTrait;

    /**
     * @var string
     */
    protected $icon = "fas fa-calendar-plus";

    /**
     * @var string[]
     */
    protected $data = [
        'load' => 'picker::datetime',
        'toggle' => 'datetimepicker'
    ];

    /**
     * @var array
     */
    protected $params = [
        ['autocomplete' => 'off']
    ];

    /**
     * On build
     */
    protected function on_build()
    {
        $this->data['target'] = "#{$this->field_id}";
    }
}