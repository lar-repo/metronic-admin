<?php

namespace Lar\MetronicAdmin\Segments\Tagable\Fields;

/**
 * Class Numeric
 * @package Lar\MetronicAdmin\Segments\Tagable\Fields
 */
class Numeric extends Input
{
    /**
     * @var string
     */
    protected $icon = "fas fa-hashtag";

    /**
     * @var string[]
     */
    protected $data = [
        'load' => 'mask',
        'load-params' => '9{0,}'
    ];
}