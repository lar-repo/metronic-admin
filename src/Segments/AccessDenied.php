<?php

namespace Lar\MetronicAdmin\Segments;

use Lar\Layout\Tags\DIV;

/**
 * Class AccessDenied
 * @package Lar\MetronicAdmin\Segments
 */
class AccessDenied extends Container {

    /**
     * AccessDenied constructor.
     */
    public function __construct()
    {
        parent::__construct(function (DIV $div) {
            $div->alert(
                'metronic.error',
                __('metronic.access_denied'),
                'fas fa-exclamation-triangle'
            )->danger();
        });
    }
}