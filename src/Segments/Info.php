<?php

namespace Lar\MetronicAdmin\Segments;

use Lar\Layout\Tags\DIV;
use Lar\MetronicAdmin\Segments\Tagable\Card;
use Lar\MetronicAdmin\Segments\Tagable\ModelInfoTable;

/**
 * Class Info
 * @package Lar\MetronicAdmin\Segments
 */
class Info extends Container {

    /**
     * Matrix constructor.
     * @param  \Closure|string  $title
     * @param  \Closure|array|null  $warp
     */
    public function __construct($title, $warp = null)
    {
        if (is_embedded_call($title)) {
            $warp = $title;
            $title = 'metronic.information';
        }

        parent::__construct(function (DIV $div) use ($title, $warp) {
            $card = null;
            $div->card($title)->haveLink($card)
                ->defaultTools()
                ->foolBody()->model_info_table(function (ModelInfoTable $table) use ($warp, $card) {
                    if (is_embedded_call($warp)) {
                        embedded_call($warp, [
                            ModelInfoTable::class => $table,
                            Card::class => $card,
                            static::class => $this
                        ]);
                    }
                });
        });
    }
}