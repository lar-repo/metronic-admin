<?php

namespace Lar\MetronicAdmin\Segments;

use Lar\Layout\Tags\DIV;
use Lar\MetronicAdmin\Segments\Tagable\Card;
use Lar\MetronicAdmin\Segments\Tagable\Form;

/**
 * Class Matrix
 * @package Lar\MetronicAdmin\Segments
 */
class Matrix extends Container {

    /**
     * Matrix constructor.
     * @param  \Closure|string|array  $title
     * @param  \Closure|array|null  $warp
     */
    public function __construct($title, $warp = null)
    {
        if (is_embedded_call($title)) {
            $warp = $title;
            $title = ['metronic.add', 'metronic.id_edit'];
        }

        if (is_array($title)) {

            $title = metronic_model_type('create') ?
                (isset($title[0]) ? $title[0] : 'metronic.add') :
                (isset($title[1]) ? $title[1] : 'metronic.id_edit');
        }

        parent::__construct(function (DIV $div) use ($title, $warp) {
            $card = null;
            $div->card($title)->haveLink($card)
                ->defaultTools()
                ->bodyForm(function (Form $form) use ($warp, $card) {
                    if (is_embedded_call($warp)) {
                        embedded_call($warp, [
                            Form::class => $form,
                            Card::class => $card,
                            static::class => $this
                        ]);
                    }
                })->footerForm();
        });
    }
}