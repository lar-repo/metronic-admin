<?php

namespace Lar\MetronicAdmin\Segments;

use Lar\Layout\Tags\DIV;
use Lar\MetronicAdmin\Segments\Tagable\Card;
use Lar\MetronicAdmin\Segments\Tagable\ModelTable;

/**
 * Class Sheet
 * @package Lar\MetronicAdmin\Segments
 */
class Sheet extends Container {

    /**
     * Sheet constructor.
     * @param  \Closure|string  $title
     * @param  \Closure|array|null  $warp
     */
    public function __construct($title, $warp = null)
    {
        if (is_embedded_call($title)) {
            $warp = $title;
            $title = 'metronic.list';
        }

        if (request()->has('show_deleted')) {
            $title = 'metronic.deleted';
        }

        parent::__construct(function (DIV $div) use ($title, $warp) {
            /** @var Card $card */
            $card = null;
            $tableObj = null;
            $div->card($title)->haveLink($card)
                ->defaultTools()->bodyModelTable(function (ModelTable $table) use ($warp, $card, &$tableObj) {
                    if (is_embedded_call($warp)) {
                        embedded_call($warp, [
                            ModelTable::class => $table,
                            Card::class => $card,
                            static::class => $this
                        ]);
                    }
                    $tableObj = $table;
                });
            $card->toolsObj(function (DIV $div) use ($tableObj) {
                $div->view('metronic::segment.model_table_actions', $tableObj->getActionData());
            });
        });
    }
}