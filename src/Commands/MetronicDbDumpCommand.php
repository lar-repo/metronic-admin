<?php

namespace Lar\MetronicAdmin\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Lar\EntityCarrier\Core\Entities\DocumentorEntity;
use Lar\MetronicAdmin\Core\ModelSaver;
use Lar\MetronicAdmin\Models\MetronicFileStorage;
use Lar\MetronicAdmin\Models\MetronicFunction;
use Lar\MetronicAdmin\Models\MetronicPermission;
use Lar\MetronicAdmin\Models\MetronicRole;
use Lar\MetronicAdmin\Models\MetronicUser;

/**
 * Class MetronicDbDumpCommand
 * @package Lar\MetronicAdmin\Commands
 */
class MetronicDbDumpCommand extends Command
{
    /**
     * @var string
     */
    static $file_name = "MetronicAdminDumpSeeder";

    /**
     * @var array
     */
    protected static $models = [];

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'metronic:db_dump';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make metronic admin db seeds';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        static::$models = array_merge([
            MetronicRole::class,
            MetronicPermission::class,
            config('metronic.auth.providers.metronic.model'),
            MetronicFileStorage::class,
            MetronicFunction::class,
        ], static::$models);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \ReflectionException
     */
    public function handle()
    {
        if (class_exists(static::$file_name)) {

            $this->class_exists(static::$file_name);
        }

        $class = class_entity(static::$file_name);
        $class->doc(function (DocumentorEntity $doc) {
            $doc->description(static::$file_name . " Class");
            $doc->tagCustom('date', now()->toDateTimeString());
        });
        $class->offAutoUse();
        $class->wrap('php');
        $class->use(Seeder::class);
        $class->use(ModelSaver::class);
        $class->extend("Seeder");

        /** @var Model $model */
        foreach (static::$models as $key => $model) {
            $model = new $model;
            static::$models[$key] = $model;
            if (method_exists($model, 'scopeMakeDumpedModel')) {
                $data = $model::makeDumpedModel();
                if ($data !== false) {
                    $class->prop('protected:' . $model->getTable(), entity(array_entity($data)->minimized()->render()));
                    $class->use(get_class($model));
                } else {
                    unset(static::$models[$key]);
                }
            }
        }

        $method = $class->method('run')
            ->docDescription('Run the database seeds.')
            ->docReturnType('void')
            ->line()
            ->line("\DB::statement('SET FOREIGN_KEY_CHECKS=0;');");

        foreach (static::$models as $model) {
            $model_name = class_basename(get_class($model));
            $method->line("ModelSaver::doMany($model_name::class, \$this->".$model->getTable().");");
        }
        $method->line("\DB::statement('SET FOREIGN_KEY_CHECKS=1;');");

        $render = $class->render();

        $file = database_path("seeds/".static::$file_name.".php");

        file_put_contents($file, $render);

        $this->info("Dump created on ".static::$file_name." seed class.");
    }

    /**
     * @param  string  $class
     * @throws \ReflectionException
     */
    protected function class_exists(string $class)
    {
        $ref = new \ReflectionClass($class);

        $date = get_doc_var($ref->getDocComment(), 'date');

        if ($date) {

            $name = str_replace(['-',' ',':'], '_', $date);

            if (!is_dir(database_path("seeds/MetronicAdminDumps"))) {
                mkdir(database_path("seeds/MetronicAdminDumps"), 0777, true);
            }

            file_put_contents(
                database_path("seeds/MetronicAdminDumps/{$name}.dump"),
                file_get_contents($ref->getFileName())
            );
        }
    }

    /**
     * @param  string  $model
     */
    public static function addModel(string $model)
    {
        static::$models[] = $model;
    }
}
