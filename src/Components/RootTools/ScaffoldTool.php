<?php

namespace Lar\MetronicAdmin\Components\RootTools;

use Lar\MetronicAdmin\Components\Vue\ScaffoldTools;
use Lar\MetronicAdmin\Segments\Tagable\Col;
use Lar\MetronicAdmin\Segments\Tagable\Field;
use Lar\MetronicAdmin\Segments\Tagable\ModelInfoTable;
use Lar\MetronicAdmin\Segments\Tagable\Row;
use Lar\MetronicAdmin\Segments\Tagable\TabContent;

/**
 * Class GeneralInformationTool
 * @package Lar\MetronicAdmin\Components\RootTools
 */
class ScaffoldTool extends TabContent
{
    /**
     * @var string
     */
    protected $icon = "fas fa-cubes";

    /**
     * @var string
     */
    protected $title = "Scaffolding";

    /**
     * @var array
     */
    public $execute = [
        'build'
    ];

    /**
     * Build tab
     */
    protected function build()
    {
        $f = array_merge(['none'], array_keys(Field::$form_components));
        $this->appEnd(ScaffoldTools::create(['fields' => $f]));
    }
}