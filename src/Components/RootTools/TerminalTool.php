<?php

namespace Lar\MetronicAdmin\Components\RootTools;

use Lar\MetronicAdmin\Components\Vue\ScaffoldTools;
use Lar\MetronicAdmin\Components\Vue\TerminalTools;
use Lar\MetronicAdmin\Segments\Tagable\Col;
use Lar\MetronicAdmin\Segments\Tagable\Field;
use Lar\MetronicAdmin\Segments\Tagable\ModelInfoTable;
use Lar\MetronicAdmin\Segments\Tagable\Row;
use Lar\MetronicAdmin\Segments\Tagable\TabContent;

/**
 * Class GeneralInformationTool
 * @package Lar\MetronicAdmin\Components\RootTools
 */
class TerminalTool extends TabContent
{
    /**
     * @var string
     */
    protected $icon = "fas fa-terminal";

    /**
     * @var string
     */
    protected $title = "Terminal";

    /**
     * @var string[]
     */
    protected $props = [
        'tab-pane',
        'role' => 'tabpanel',
    ];

    /**
     * @var array
     */
    public $execute = [
        'build'
    ];

    /**
     * Build tab
     */
    protected function build()
    {
        $f = array_merge(['none'], array_keys(Field::$form_components));
        $this->appEnd(TerminalTools::create());
    }
}