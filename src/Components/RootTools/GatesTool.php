<?php

namespace Lar\MetronicAdmin\Components\RootTools;

use Lar\MetronicAdmin\Components\Vue\GatesTools;
use Lar\MetronicAdmin\Models\MetronicFunction;
use Lar\MetronicAdmin\Models\MetronicRole;
use Lar\MetronicAdmin\Resources\MetronicFunctionResource;
use Lar\MetronicAdmin\Segments\Tagable\TabContent;

/**
 * Class GatesTool
 * @package Lar\MetronicAdmin\Components\RootTools
 */
class GatesTool extends TabContent
{
    /**
     * @var string
     */
    protected $icon = "fas fa-key";

    /**
     * @var string
     */
    protected $title = "Preferences";

    /**
     * @var array
     */
    public $execute = [
        'build'
    ];

    /**
     * Build tab
     */
    protected function build()
    {
        $action = \Str::parseCallback(\Route::currentRouteAction());

        if (metronic_now()) {
            $this->appEnd(GatesTools::create([
                'metronic' => metronic_now(),
                'roles' => MetronicRole::all(),
                'action' => $action,
                'funcs' => MetronicFunctionResource::collection(
                    MetronicFunction::with('roles')->where('class', trim($action[0], '\\'))->get()
                )->toArray(request())
            ]));
        } else {
            $this->div()->textCenter()->textMuted()->w100()
                ->h3('No roles available!');
        }
    }
}