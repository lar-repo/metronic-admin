<?php

namespace Lar\MetronicAdmin\Components\Vue;

use Lar\Tagable\Vue;

/**
 * Class Navigator
 * @package Lar\MetronicAdmin\Components\Vue
 */
class Navigator extends Vue
{
    /**
     * @var string
     */
    protected $element = "v-navigator";
}