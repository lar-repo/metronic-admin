<?php

namespace Lar\MetronicAdmin\Components\Vue;

use Lar\Tagable\Vue;

/**
 * Class GlobalSearch
 * @package Lar\MetronicAdmin\Components\Vue
 */
class ScaffoldTools extends Vue
{
    /**
     * @var string
     */
    protected $element = "scaffold_tools";
}