<?php

namespace Lar\MetronicAdmin\Components\Vue;

use Lar\Tagable\Vue;

/**
 * Class LiveReloader
 * @package Lar\MetronicAdmin\Components\Vue
 */
class LiveReloader extends Vue
{
    /**
     * @var string
     */
    protected $element = "live_reloader";
}