<?php

namespace Lar\MetronicAdmin\Components\Vue;

use Lar\Tagable\Vue;

/**
 * Class GlobalSearch
 * @package Lar\MetronicAdmin\Components\Vue
 */
class GatesTools extends Vue
{
    /**
     * @var string
     */
    protected $element = "gate_tools";
}