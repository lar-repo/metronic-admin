<?php

namespace Lar\MetronicAdmin\Components\Vue;

use Lar\Tagable\Vue;

/**
 * Class GlobalSearch
 * @package Lar\MetronicAdmin\Components\Vue
 */
class TerminalTools extends Vue
{
    /**
     * @var string
     */
    protected $element = "terminal_tools";
}