<?php

namespace Lar\MetronicAdmin\Components\Vue;

use Lar\Tagable\Vue;

/**
 * Class FormActionAfterSave
 * @package Lar\MetronicAdmin\Components\Vue
 */
class FormActionAfterSave extends Vue
{
    /**
     * @var string
     */
    protected $element = "form_action_after_save";
}