<?php

namespace Lar\MetronicAdmin\Components\Vue;

use Lar\Tagable\Vue;

/**
 * Class ModalCollection
 * @package Lar\MetronicAdmin\Components\Vue
 */
class ModalCollection extends Vue
{
    /**
     * @var string
     */
    protected $element = "v-modal-collection";
}