<?php

namespace Lar\MetronicAdmin\Components\Vue;

use Lar\Tagable\Vue;

/**
 * Class GlobalSearch
 * @package Lar\MetronicAdmin\Components\Vue
 */
class GlobalSearch extends Vue
{
    /**
     * @var string
     */
    protected $element = "global_search";
}