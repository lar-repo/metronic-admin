<?php


namespace Lar\MetronicAdmin\Components;

use Lar\Layout\Tags\DIV;
use Lar\MetronicAdmin\Components\RootTools\GatesTool;
use Lar\MetronicAdmin\Components\RootTools\GeneralInformationTool;
use Lar\MetronicAdmin\Components\RootTools\ScaffoldTool;
use Lar\MetronicAdmin\Components\RootTools\TerminalTool;

/**
 * Class RootTools
 * @package Lar\MetronicAdmin\Components
 */
class RootTools extends DIV
{
    /**
     * @var array
     */
    static $tabs = [
        GeneralInformationTool::class,
        GatesTool::class,
        ScaffoldTool::class,
        //TerminalTool::class
    ];

    /**
     * @var string[]
     */
    protected $props = [
        'id' => 'root-tools',
        'collapse', 'container-fluid'
    ];

    /**
     * @var string[]
     */
    public $execute = [
        "build"
    ];

    /**
     * Build tools
     */
    protected function build()
    {
        $this->row()->mb5()
            ->col()->mb5()
            ->card('Root tools')->success()
            ->tabs()
            ->tabList(static::$tabs);
    }
}