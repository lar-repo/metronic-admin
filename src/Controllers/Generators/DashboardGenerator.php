<?php

namespace Lar\MetronicAdmin\Controllers\Generators;

use Composer\Composer;
use Lar\Developer\Generator;
use Lar\Layout\Abstracts\Component;
use Lar\Layout\Tags\DIV;
use Lar\MetronicAdmin\Segments\Tagable\Row;
use PDO;

/**
 * Class DashboardGenerator
 * @package Lar\MetronicAdmin\Controllers\Generators
 */
class DashboardGenerator extends Generator
{
    /**
     * @var DIV
     */
    public $div = DIV::class;

    /**
     * @var array
     */
    protected $required = [
        'redis', 'xml', 'xmlreader', 'openssl', 'bcmath', 'json', 'mbstring', 'session', 'mysqlnd', 'PDO', 'pdo_mysql', 'Phar', 'mysqli', 'SimpleXML', 'sockets', 'exif'
    ];

    /**
     * @return $this
     */
    public function aboutServer()
    {
        $this->div->row(function (Row $row) {
            $row->col(6)->mb4()
                ->card('metronic.environment')->h100()
                ->foolBody(['table-responsive'])->table($this->environmentInfo());

            $row->col(6)->mb4()
                ->card('Laravel')->h100()
                ->foolBody(['table-responsive'])->table($this->laravelInfo());

            $row->col(6)->mb4()
                ->card('Composer')->h100()
                ->foolBody(['table-responsive'])->table($this->composerInfo());

            $row->col(6)->mb4()
                ->card('metronic.database')->h100()
                ->foolBody(['table-responsive'])->table($this->databaseInfo());
        });

        return $this;
    }

    /**
     * @return array
     */
    protected function environmentInfo()
    {
        $mods = get_loaded_extensions();

        foreach ($mods as $key => $mod) {

            if (array_search($mod, $this->required) !== false) {

                $mods[$key] = "<span class=\"badge badge-success\">{$mod}</span>";
            }

            else {

                $mods[$key] = "<span class=\"badge badge-warning\">{$mod}</span>";
            }
        }

        return [
            __('metronic.php_version') =>  "<span class=\"badge badge-dark\">v".versionString(PHP_VERSION)."</span>",
            __('metronic.php_modules') => implode(', ', $mods),
            __('metronic.cgi') => php_sapi_name(),
            __('metronic.os') => php_uname(),
            __('metronic.server') => \Arr::get($_SERVER, 'SERVER_SOFTWARE'),
            __('metronic.root') => \Arr::get($_SERVER, 'DOCUMENT_ROOT'),
            'System Load Average' => function_exists('sys_getloadavg') ? sys_getloadavg()[0] : 0
        ];
    }

    /**
     * @return array
     */
    protected function laravelInfo()
    {
        $user_model = config('auth.providers.users.model');
        $metronic_user_model = config('metronic.auth.providers.metronic.model');

        return [
            __('metronic.laravel_version') =>  "<span class=\"badge badge-dark\">v".versionString(\App::version())."</span>",
            __('metronic.metronic_version') =>   "<span class=\"badge badge-dark\">v".versionString(\MetronicAdmin::version())."</span>",
            __('metronic.timezone') => config('app.timezone'),
            __('metronic.language') => config('app.locale'),
            __('metronic.languages_involved') => implode(', ', config('layout.languages')),
            __('metronic.env') => config('app.env'),
            __('metronic.url') => config('app.url'),
            __('metronic.users') => number_format($user_model::count(), 0, '', ','),
            __('metronic.metronic_users') => number_format($metronic_user_model::count(), 0, '', ','),
            '' => function (Component $component) {
                $component->_addClass(['table-secondary']);
                $component->_find('th')->h6(['m-0'], __('metronic.drivers'));
            },
            __('metronic.broadcast_driver') => "<span class=\"badge badge-secondary\">".config('broadcasting.default')."</span>",
            __('metronic.cache_driver') => "<span class=\"badge badge-secondary\">".config('cache.default')."</span>",
            __('metronic.session_driver') => "<span class=\"badge badge-secondary\">".config('session.driver')."</span>",
            __('metronic.queue_driver') => "<span class=\"badge badge-secondary\">".config('queue.default')."</span>",
            __('metronic.mail_driver') => "<span class=\"badge badge-secondary\">".config('mail.driver')."</span>",
            __('metronic.hashing_driver') => "<span class=\"badge badge-secondary\">".config('hashing.driver')."</span>",
            __('metronic.hashing_driver') => "<span class=\"badge badge-secondary\">".config('hashing.driver')."</span>",
        ];
    }

    /**
     * @return array
     */
    protected function composerInfo()
    {
        $return = [
            __('metronic.composer_version') =>  "<span class=\"badge badge-dark\">v".versionString(Composer::getVersion())."</span>",
            '' => function (Component $component) {
                $component->_addClass(['table-secondary']);
                $component->_find('th')->h6(['m-0'], __('metronic.required'));
            },
        ];

        $json = file_get_contents(base_path('composer.json'));

        $dependencies = json_decode($json, true)['require'];

        foreach ($dependencies as $name => $ver) {

            $return[$name] = "<span class=\"badge badge-dark\">{$ver}</span>";
        }

        return $return;
    }

    /**
     * @return array
     */
    protected function databaseInfo()
    {
        /** @var \PDO $pdo */
        $pdo = \DB::query("SHOW VARIABLES")->getConnection()->getPdo();

        return [
            __('metronic.server_version') => "<span class=\"badge badge-dark\">v".versionString($pdo->getAttribute(\PDO::ATTR_SERVER_VERSION))."</span>",
            __('metronic.client_version') => $pdo->getAttribute(\PDO::ATTR_CLIENT_VERSION),
            __('metronic.server_info') => $pdo->getAttribute(\PDO::ATTR_SERVER_INFO),
            __('metronic.connection_status') => $pdo->getAttribute(\PDO::ATTR_CONNECTION_STATUS),
            __('metronic.mysql_driver') => $pdo->getAttribute(\PDO::ATTR_DRIVER_NAME),
            '' => function (Component $component) {
                $component->_addClass(['table-secondary']);
                $component->_find('th')->h6(['m-0'], __('metronic.connection_info'));
            },
            __('metronic.db_driver') => config('database.default'),
            __('metronic.database') => env('DB_DATABASE'),
            __('metronic.user') => env('DB_USERNAME'),
            __('metronic.password') => str_repeat('*', strlen(env('DB_PASSWORD'))),
        ];
    }
}