<?php

namespace Lar\MetronicAdmin\Controllers\Administrators;

use Lar\MetronicAdmin\Controllers\AdministratorsController;
use Lar\MetronicAdmin\Models\MetronicUser;
use Lar\MetronicAdmin\Segments\Sheet;
use Lar\MetronicAdmin\Segments\Tagable\ModelTable;

/**
 * Class IndexController
 * @package Lar\MetronicAdmin\Controllers\Administrators
 */
class IndexController extends AdministratorsController {

    /**
     * @return Sheet
     */
    public function index()
    {
        return Sheet::create('metronic.admin_list', function (ModelTable $table) {

            $table->search->id();
            $table->search->email('email', 'metronic.email_address');
            $table->search->input('login', 'metronic.login_name', '=%');
            $table->search->input('name', 'metronic.name', '=%');
            $table->search->at();

            $table->id();
            $table->column('metronic.avatar', 'avatar')->avatar();
            $table->column('metronic.role', [$this, 'show_role']);
            $table->column('metronic.email_address', 'email')->sort();
            $table->column('metronic.login_name', 'login')->sort();
            $table->column('metronic.name', 'name')->sort();
            $table->at();
            $table->controlDelete(function (MetronicUser $user) { return $user->id !== 1 && admin()->id !== $user->id; });
            $table->disableChecks();
        });
    }
}