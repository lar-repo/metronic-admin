<?php

namespace Lar\MetronicAdmin\Controllers\Administrators;

use Lar\MetronicAdmin\Controllers\AdministratorsController;
use Lar\MetronicAdmin\Models\MetronicRole;
use Lar\MetronicAdmin\Models\MetronicUser;
use Lar\MetronicAdmin\Segments\Matrix;
use Lar\MetronicAdmin\Segments\Tagable\Card;
use Lar\MetronicAdmin\Segments\Tagable\Form;
use Lar\MetronicAdmin\Segments\Tagable\TabContent;

/**
 * Class MatrixController
 * @package Lar\MetronicAdmin\Controllers\Administrators
 */
class MatrixController extends AdministratorsController
{
    /**
     * @return Matrix
     */
    public function matrix()
    {
        return new Matrix(['metronic.add_admin', 'metronic.edit_admin'], function (Form $form, Card $card) {

            $card->defaultTools(function ($type) {
                return $type === 'delete' && $this->model()->id == 1 && admin()->id == $this->model()->id ? false : true;
            });

            $form->info_id();

            $form->image('avatar', 'metronic.avatar')->nullable();

            $form->tab('metronic.common', 'fas fa-cogs', function (TabContent $tab)  {

                $tab->input('login', 'metronic.login_name')
                    ->required()
                    ->unique(MetronicUser::class, 'login', $this->model()->id);

                $tab->input('name', 'metronic.name')->required();

                $tab->email('email', 'metronic.email_address')
                    ->required()->unique(MetronicUser::class, 'email', $this->model()->id);

                $tab->multi_select('roles[]', 'metronic.role')->icon_user_secret()
                    ->options(MetronicRole::all()->pluck('name','id'));
            });

            $form->tab('metronic.password', 'fas fa-key', function (TabContent $tab)  {

                $tab->password('password', 'metronic.new_password')
                    ->confirm()->required_condition($this->isType('create'));
            });

            $form->info_at();
        });
    }
}