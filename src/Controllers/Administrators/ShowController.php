<?php

namespace Lar\MetronicAdmin\Controllers\Administrators;

use Lar\MetronicAdmin\Controllers\AdministratorsController;
use Lar\MetronicAdmin\Segments\Info;
use Lar\MetronicAdmin\Segments\Tagable\Card;
use Lar\MetronicAdmin\Segments\Tagable\ModelInfoTable;

/**
 * Class ShowController
 * @package Lar\MetronicAdmin\Controllers\Administrators
 */
class ShowController extends AdministratorsController
{
    /**
     * @return Info
     */
    public function show()
    {
        return Info::create(function (ModelInfoTable $table, Card $card) {
            $card->defaultTools(function ($type) {
                return $type === 'delete' && $this->model()->id == 1 ? false : true;
            });

            $table->row('metronic.avatar', 'avatar')->avatar(150);
            $table->row('metronic.role', [$this, 'show_role']);
            $table->row('metronic.email_address', 'email');
            $table->row('metronic.login_name', 'login');
            $table->row('metronic.name', 'name');
            $table->at();
        });
    }
}