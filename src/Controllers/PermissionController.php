<?php

namespace Lar\MetronicAdmin\Controllers;

use Lar\MetronicAdmin\Models\MetronicPermission;
use Lar\MetronicAdmin\Segments\Info;
use Lar\MetronicAdmin\Segments\Matrix;
use Lar\MetronicAdmin\Segments\Sheet;
use Lar\MetronicAdmin\Segments\Tagable\Form;
use Lar\MetronicAdmin\Segments\Tagable\ModelInfoTable;
use Lar\MetronicAdmin\Segments\Tagable\ModelTable;

/**
 * Class HomeController
 *
 * @package Lar\MetronicAdmin\Controllers
 */
class PermissionController extends Controller
{
    /**
     * @var string
     */
    static $model = \Lar\MetronicAdmin\Models\MetronicPermission::class;

    /**
     * @var string[]
     */
    public $method_colors = [
        '*' => 'primary',
        'GET' => 'success',
        'HEAD' => 'secondary',
        'POST' => 'danger',
        'PUT' => 'warning',
        'PATCH' => 'info',
        'DELETE' => 'light',
        'OPTIONS' => 'dark'
    ];

    /**
     * @return Sheet
     */
    public function index()
    {
        return Sheet::create(function (ModelTable $table) {

            $table->search->id();
            $table->search->input('path', 'metronic.path');
            $table->search->select('metronic_role_id', 'metronic.role')
                ->options(\Lar\MetronicAdmin\Models\MetronicRole::all()->pluck('name', 'id'))->nullable();
            $table->search->at();

            $table->id();
            $table->column('metronic.description', 'description')->str_limit(50);
            $table->column('metronic.path', 'path')->badge('success');
            $table->column('metronic.methods', [$this, 'show_methods'])->sort('method');
            $table->column('metronic.state', [$this, 'show_state'])->sort('state');
            $table->column('metronic.role', 'role.name')->sort('role_id');
            $table->active_switcher();
            $table->at();
        });
    }

    /**
     * @return Matrix
     */
    public function matrix()
    {
        return Matrix::create(function (Form $form) {

            $form->info_id();

            $form->input('path', 'metronic.path')
                ->required();

            $form->multi_select('method[]', 'metronic.methods')
                ->options(collect(array_merge(['*'], \Illuminate\Routing\Router::$verbs))->mapWithKeys(function($i) {return [$i => $i];})->toArray())
                ->required();

            $form->radios('state', 'metronic.state')
                ->options(['close' => __('metronic.close'), 'open' => __('metronic.open')], true)
                ->required();

            $form->radios('metronic_role_id', 'metronic.role')
                ->options(\Lar\MetronicAdmin\Models\MetronicRole::all()->pluck('name', 'id'), true)
                ->required();

            $form->input('description', 'metronic.description');

            $form->switcher('active', 'metronic.active')->switchSize('mini')
                ->default(1);

            $form->info_at();
        });
    }

    /**
     * @return Info
     */
    public function show()
    {
        return Info::create(function (ModelInfoTable $table) {
            $table->id();
            $table->row('metronic.path', 'path')->badge('success');
            $table->row('metronic.methods', [$this, 'show_methods']);
            $table->row('metronic.state', [$this, 'show_state']);
            $table->row('metronic.role', 'role.name');
            $table->row('metronic.active', 'active')->yes_no();
            $table->at();
        });
    }

    /**
     * @param  MetronicPermission  $permission
     * @return string
     */
    public function show_methods(MetronicPermission $permission)
    {
        return collect($permission->method)->map(function ($i) {
            return "<span class=\"badge badge-{$this->method_colors[$i]}\">{$i}</span>";
        })->implode(' ');
    }

    /**
     * @param  MetronicPermission  $permission
     * @return string
     */
    public function show_state(MetronicPermission $permission)
    {
        return "<span class=\"badge badge-".($permission->state === 'open' ? 'success' : 'danger')."\">".($permission->state === 'open' ? '<i class="fas fa-check-circle"></i>' : '<i class="fas fa-times-circle"></i>')." ".__("metronic.{$permission->state}")."</span>";
    }
}
