<?php

namespace Lar\MetronicAdmin\Controllers;

use Illuminate\Http\Request;

/**
 * Class AuthController
 *
 * @package Lar\MetronicAdmin\Controllers
 */
class AuthController
{
    /**
     * Make login page
     */
    public function login()
    {
        if (!\MetronicAdmin::guest()) {

            return redirect()->route('metronic.dashboard');
        }

        return view('metronic::auth.login');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function login_post(Request $request)
    {
        $request->validate([
            'login' => 'required|min:3|max:191',
            'password' => 'required|min:4|max:191',
        ]);

        $login = false;

        if (\Auth::guard('metronic')->attempt(['login' => $request->login, 'password' => $request->password], $request->remember=='on' ? true : false)) {

            $request->session()->regenerate();

            \respond()->toast_success("User success auth by Login");

            $login = true;
        }

        else if (\Auth::guard('metronic')->attempt(['email' => $request->login, 'password' => $request->password], $request->remember=='on' ? true : false)) {

            $request->session()->regenerate();

            \respond()->toast_success("User success auth by E-Mail");

            $login = true;
        }

        else {

            \respond()->toast_error("User not found!");
        }

        if ($login && session()->has('return_authenticated_url')) {

            return redirect(session()->pull('return_authenticated_url'));
        }

        return redirect($request->headers->get('referer'));
    }
}
