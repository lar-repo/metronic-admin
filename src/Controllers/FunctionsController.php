<?php

namespace Lar\MetronicAdmin\Controllers;

use Lar\MetronicAdmin\Models\MetronicFunction;
use Lar\MetronicAdmin\Models\MetronicRole;
use Lar\MetronicAdmin\Segments\Info;
use Lar\MetronicAdmin\Segments\Matrix;
use Lar\MetronicAdmin\Segments\Sheet;
use Lar\MetronicAdmin\Segments\Tagable\Card;
use Lar\MetronicAdmin\Segments\Tagable\Form;
use Lar\MetronicAdmin\Segments\Tagable\ModelInfoTable;
use Lar\MetronicAdmin\Segments\Tagable\ModelTable;

/**
 * Class HomeController
 *
 * @package Lar\MetronicAdmin\Controllers
 */
class FunctionsController extends Controller
{
    /**
     * @var string
     */
    static $model = MetronicFunction::class;

    /**
     * @var array
     */
    static $roles = ['root'];

    /**
     * @return Sheet
     */
    public function index()
    {
        return Sheet::create(function (ModelTable $table, Card $card) {

            $table->search->id();
            $table->search->input('slug', 'metronic.slug');
            $table->search->input('class', 'Class', '%=%');
            $table->search->at();

            $table->id();
            $table->column('metronic.role', [$this, 'show_roles']);
            $table->column('metronic.slug', 'slug')->sort()->input_editable()
                ->copied()->to_prepend_link('fas fa-glasses', null, '{class}');
            $table->column('metronic.description', 'description')->to_lang()->has_lang()->str_limit(50)->textarea_editable()->sort();
            $table->active_switcher();
            $table->at();


        });
    }

    /**
     * @return Matrix
     */
    public function matrix()
    {
        return Matrix::create(function (Form $form) {
            $form->info_id();
            $form->input('slug', 'metronic.slug')->required()
                ->slugable();
            $form->checks('roles', 'metronic.roles')->required()
                ->options(MetronicRole::all()->pluck('name', 'id'));
            $form->textarea('description', 'metronic.description');
            $form->switcher('active', 'metronic.active')->boolean();
            $form->info_at();
        });
    }

    /**
     * @return Info
     */
    public function show()
    {
        return Info::create(function (ModelInfoTable $table) {
            $table->id();
            $table->row('metronic.role', [$this, 'show_roles']);
            $table->row('metronic.slug', 'slug')->copied();
            $table->row('metronic.description', 'description')->to_lang()->has_lang()->str_limit(50);
            $table->row('metronic.active', 'active')->input_switcher();
            $table->at();
        });
    }

    /**
     * @param  MetronicFunction  $function
     * @return string
     */
    public function show_roles(MetronicFunction $function)
    {
        return '<span class="badge badge-success">' . $function->roles->pluck('name')->implode('</span> <span class="badge badge-success">') . '</span>';
    }
}
