<?php

namespace Lar\MetronicAdmin\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Lar\MetronicAdmin\Core\ModelSaver;
use Illuminate\Routing\Controller;
use Lar\MetronicAdmin\ExtendProvider;
use Lar\MetronicAdmin\Models\MetronicRole;

/**
 * Trait ControllerMethods
 * @package Lar\MetronicAdmin\Core\Traits
 */
abstract class BaseController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var ExtendProvider|null
     */
    static $extension_affiliation;

    /**
     * @var bool
     */
    static $no_getter_model = false;

    /**
     * Save request to model
     *
     * @param  array|null  $data
     * @return bool|void
     */
    public function requestToModel(array $data = null)
    {
        $save = $data ?? request()->all();

        foreach (static::$crypt_fields as $crypt_field) {
            if (array_key_exists($crypt_field, $save)) {
                if ($save[$crypt_field]) {
                    $save[$crypt_field] = bcrypt($save[$crypt_field]);
                } else {
                    unset($save[$crypt_field]);
                }
            }
        }

        return $this->model() ? ModelSaver::do($this->model(), $save) : false;
    }

    /**
     * Get only exists model
     *
     * @return \Illuminate\Database\Eloquent\Model|\Lar\MetronicAdmin\Getters\Menu|string|null
     */
    public function existsModel()
    {
        return $this->model() && $this->model()->exists ? $this->model() : null;
    }

    /**
     * Get menu model
     *
     * @return \Illuminate\Database\Eloquent\Model|\Lar\MetronicAdmin\Getters\Menu|string|null
     */
    public function model()
    {
        return !static::$no_getter_model ? gets()->metronic->menu->model : null;
    }

    /**
     * Get model primary
     *
     * @return \Lar\MetronicAdmin\Getters\Menu|object|string|null
     */
    public function model_primary()
    {
        return gets()->metronic->menu->model_primary;
    }

    /**
     * Get now menu
     *
     * @return array|\Lar\MetronicAdmin\Getters\Menu|null
     */
    public function now()
    {
        return gets()->metronic->menu->now;
    }

    /**
     * Get resource type
     *
     * @return \Lar\MetronicAdmin\Getters\Menu|string|null
     */
    public function type()
    {
        return gets()->metronic->menu->type;
    }

    /**
     * Check type for resource
     *
     * @param  string  $type
     * @return bool
     */
    public function isType(string $type)
    {
        return $this->type() === $type;
    }

    /**
     * @param  null  $name
     * @param  null  $default
     * @return array|mixed
     */
    public function data($name = null, $default = null)
    {
        if (!$name) {

            return gets()->metronic->menu->data;
        }

        return gets()->metronic->menu->data($name, $default);
    }

    /**
     * @param  string  $method
     * @return bool
     */
    public function can(string $method)
    {
        return metronic_controller_can(static::class, $method);
    }

    /**
     * Get the map of resource methods to ability names.
     *
     * @return array
     */
    protected function resourceMap()
    {
        return [
            'index' => 'Index',
            'show' => 'Show',
            'create' => 'Create',
            'store' => 'Store',
            'edit' => 'Edit',
            'update' => 'Update',
            'destroy' => 'Destroy',
        ];
    }

    /**
     * @param  string  $method
     * @param  array  $params
     * @return ModalController
     */
    public function new_modal(string $method, array $params = [])
    {
        return (new ModalController($params))->setHandle(static::class . "::" . $method);
    }

    /**
     * @return ExtendProvider|null
     */
    public static function extension_affiliation()
    {
        if (static::$extension_affiliation) {

            return static::$extension_affiliation;
        }

        $provider = "ServiceProvider";

        $providers = \MetronicAdmin::extensionProviders();

        $iteration = 1;

        while (!empty($piece = body_namespace_element(static::class, $iteration))) {

            if (isset($providers["{$piece}\\{$provider}"])) {

                static::$extension_affiliation = \MetronicAdmin::getExtension($providers["{$piece}\\{$provider}"]);

                break;
            }

            $iteration++;
        }

        return static::$extension_affiliation;
    }

    /**
     * @param  string  $method
     * @param  array|string[]  $roles
     * @param  string|null  $description
     * @return array
     */
    public static function generatePermission(string $method, array $roles = ['*'], string $description = null)
    {
        $provider = static::extension_affiliation();

        $p_desc = "";

        if ($provider && $provider::$description) {

            $p_desc = $provider::$description;
        }

        if (!$p_desc) {

            $p_desc = static::class;
        }

        return [
            'slug' => $method,
            'class' => static::class,
            'description' => $p_desc . ($description ? " [$description]" : (\Lang::has("metronic.about_method.{$method}") ? " [@metronic.about_method.{$method}]":" [{$method}]")),
            'roles' => $roles === ['*'] ? MetronicRole::all()->pluck('id')->toArray() : collect($roles)->map(function ($item) {
                return is_numeric($item) ? $item : MetronicRole::where('slug', $item)->first()->id;
            })->filter()->values()->toArray()
        ];
    }
}