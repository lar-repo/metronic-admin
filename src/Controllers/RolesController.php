<?php

namespace Lar\MetronicAdmin\Controllers;

use Lar\MetronicAdmin\Models\MetronicRole;
use Lar\MetronicAdmin\Segments\Info;
use Lar\MetronicAdmin\Segments\Matrix;
use Lar\MetronicAdmin\Segments\Sheet;
use Lar\MetronicAdmin\Segments\Tagable\Form;
use Lar\MetronicAdmin\Segments\Tagable\ModelTable;
use Lar\MetronicAdmin\Segments\Tagable\ModelInfoTable;

/**
 * Class HomeController
 *
 * @package Lar\MetronicAdmin\Controllers
 */
class RolesController extends Controller
{
    /**
     * @var string
     */
    static $model = MetronicRole::class;

    /**
     * @return Sheet
     */
    public function index()
    {
        return Sheet::create('metronic.list_of_roles', function (ModelTable $table) {

            //dd(admin()->can('viewAny', static::$model));

            $table->search->id();
            $table->search->input('name', 'metronic.title', '=%');
            $table->search->input('slug', 'metronic.slug', '=%');
            $table->search->at();

            $table->id();
            $table->column('metronic.title', 'name')->sort();
            $table->column('metronic.slug', 'slug')->sort()->badge('success');
            $table->at();
        });
    }

    /**
     * @return Matrix
     */
    public function matrix()
    {
        return Matrix::create(['metronic.add_role', 'metronic.edit_role'], function (Form $form) {

            $form->info_id();
            $form->input('name', 'metronic.title')->required()->duplication_how_slug('#input_slug');
            $form->input('slug', 'metronic.slug')->required()->slugable();
            $form->info_at();
        });
    }

    /**
     * @return Info
     */
    public function show()
    {
        return Info::create(function (ModelInfoTable $table) {

            $table->id();
            $table->row('metronic.title', 'name');
            $table->row('metronic.slug', 'slug')->badge('success');
            $table->at();
        });
    }
}
