<?php

namespace Lar\MetronicAdmin\Controllers;

use Lar\Layout\Tags\DIV;
use Lar\MetronicAdmin\Models\MetronicRole;
use Lar\MetronicAdmin\Models\MetronicUser;
use Lar\MetronicAdmin\Segments\Container;
use Lar\MetronicAdmin\Segments\Info;
use Lar\MetronicAdmin\Segments\Matrix;
use Lar\MetronicAdmin\Segments\Sheet;
use Lar\MetronicAdmin\Segments\Tagable\Card;
use Lar\MetronicAdmin\Segments\Tagable\Form;
use Lar\MetronicAdmin\Segments\Tagable\ModelInfoTable;
use Lar\MetronicAdmin\Segments\Tagable\ModelTable;
use Lar\MetronicAdmin\Segments\Tagable\SearchForm;
use Lar\MetronicAdmin\Segments\Tagable\TabContent;
use Lar\MetronicAdmin\Segments\Tagable\Tabs;

/**
 * Class AdministratorsController
 * @package Lar\MetronicAdmin\Controllers
 */
class AdministratorsController extends Controller
{
    /**
     * @var string
     */
    static $model = MetronicUser::class;

    /**
     * @param  MetronicUser  $user
     * @return string
     */
    public function show_role(MetronicUser $user)
    {
        return '<span class="badge badge-success">' . $user->roles->pluck('name')->implode('</span> <span class="badge badge-success">') . '</span>';
    }
}
