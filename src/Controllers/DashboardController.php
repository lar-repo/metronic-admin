<?php

namespace Lar\MetronicAdmin\Controllers;

use Lar\MetronicAdmin\Controllers\Generators\DashboardGenerator;
use Lar\MetronicAdmin\Segments\Container;

/**
 * Class DashboardController
 *
 * @package Lar\MetronicAdmin\Controllers
 */
class DashboardController extends Controller
{
    /**
     * @return Container
     */
    public function index()
    {
        return Container::create(function (DashboardGenerator $generator) {

            $generator->aboutServer();
        });
    }
}
