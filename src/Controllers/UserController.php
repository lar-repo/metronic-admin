<?php

namespace Lar\MetronicAdmin\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Lar\Layout\Respond;
use Lar\Layout\Tags\DIV;
use Lar\MetronicAdmin\Core\ModelSaver;
use Lar\MetronicAdmin\Models\MetronicUser;
use Lar\MetronicAdmin\Segments\Container;
use Lar\MetronicAdmin\Segments\Tagable\Card;
use Lar\MetronicAdmin\Segments\Tagable\Form;
use Lar\MetronicAdmin\Segments\Tagable\FormFooter;
use Lar\MetronicAdmin\Segments\Tagable\Row;
use Lar\MetronicAdmin\Segments\Tagable\Tabs;

/**
 * Class HomeController
 *
 * @package Lar\MetronicAdmin\Controllers
 */
class UserController extends Controller
{
    /**
     * @var MetronicUser
     */
    protected $user;

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|Container
     */
    public function index()
    {
        return Container::create(function (DIV $div, Container $container) {

            $container->title($this->model()->name)
                ->icon_user()
                ->breadcrumb('metronic.administrator', 'metronic.profile');

            $div->row(function (Row $row) {

                $row->col(3)
                    ->card('metronic.information')
                    ->primary()
                    ->body()
                    ->view('metronic::auth.user_portfolio', ['user' => $this->model()]);

                $card = $row->col(9)
                    ->card('metronic.edit');

                $card->success()
                    ->body($this->matrix());

                $card->footerForm(false, function (FormFooter $footer) {
                    $footer->setType('edit');
                });
            });
        });
    }

    /**
     * @return \Lar\Layout\Abstracts\Component|\Lar\Layout\LarDoc|Form
     */
    public function matrix()
    {
        return Form::create(function (Form $form) {

            $form->vertical();

            $form->image('avatar', 'metronic.avatar');

            $form->input('login', 'metronic.login_name')
                ->required()
                ->unique(MetronicUser::class, 'login', $this->model()->id);

            $form->email('email', 'metronic.email_address')
                ->required()
                ->unique(MetronicUser::class, 'email', $this->model()->id);

            $form->input('name', 'metronic.name')
                ->required();

            $form->br()->h5(__('metronic.password'))->hr();

            $form->password('password', 'metronic.new_password')
                ->confirm();
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Model|\Lar\MetronicAdmin\Getters\Menu|MetronicUser|string|null
     */
    public function model()
    {
        return admin();
    }

    /**
     * @param  Respond  $respond
     * @return Respond
     */
    public function logout(Respond $respond)
    {
        \Auth::guard('metronic')->logout();

        $respond->redirect(route('metronic.login'));

        return $respond;
    }
}
