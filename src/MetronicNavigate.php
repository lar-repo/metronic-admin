<?php

namespace Lar\MetronicAdmin;

use Illuminate\Contracts\Support\Arrayable;
use Lar\MetronicAdmin\Core\NavGroup;
use Lar\MetronicAdmin\Core\NavItem;
use Lar\MetronicAdmin\Core\Traits\NavDefaultTools;
use Lar\MetronicAdmin\Interfaces\NavigateInterface;
use Lar\Roads\Roads;

/**
 * Class MetronicNavigate
 * @package Lar\MetronicAdmin
 */
class MetronicNavigate implements NavigateInterface
{
    use NavDefaultTools;

    /**
     * @var array
     */
    public static $items = [];

    /**
     * @var Roads
     */
    public static $roads;

    /**
     * @var ExtendProvider
     */
    public static $extension;

    /**
     * @param  \Closure|array  ...$calls
     * @return $this
     */
    public static function do(...$calls)
    {
        foreach ($calls as $call) {

            call_user_func($call, \MetronicNavigate::instance(), static::$roads);
        }

        return \MetronicNavigate::instance();
    }

    /**
     * @param  string  $title
     * @return $this
     */
    public function menu_header(string $title)
    {
        MetronicNavigate::$items[] = collect(['main_header' => $title]);

        return $this;
    }

    /**
     * @param  string  $view
     * @param  array  $params
     * @param  bool  $prepend
     * @return $this
     */
    public function nav_bar_view(string $view, array $params = [], bool $prepend = false)
    {
        MetronicNavigate::$items[] = collect(['nav_bar_view' => $view, 'params' => $params, 'prepend' => $prepend]);

        return $this;
    }

    /**
     * @param  string  $view
     * @param  array  $params
     * @return $this
     */
    public function left_nav_bar_view(string $view, array $params = [])
    {
        MetronicNavigate::$items[] = collect(['left_nav_bar_view' => $view, 'params' => $params]);

        return $this;
    }

    /**
     * @param  string|null  $title
     * @param  string|null|\Closure|array  $route
     * @param  \Closure|array|null  $cb
     * @return \Lar\MetronicAdmin\Core\NavGroup
     */
    public function group(string $title = null, $route = null, $cb = null)
    {
        if (is_embedded_call($route) && !is_string($route)) {
            $cb = $route;
            $route = null;
        }

        $item = new NavGroup($title, $route);

        MetronicNavigate::$items[] = $item;

        if (isset($item->items['route'])) {

            $this->includeAfterGroup($item->items['route']);
        }

        if (is_embedded_call($cb)) {

            call_user_func($cb, $item, static::$roads);
        }

        return $item;
    }

    /**
     * @param  string|null  $title
     * @param  string|null  $route
     * @param  string|\Closure|array|null  $action
     * @return \Lar\MetronicAdmin\Core\NavItem
     */
    public function item(string $title = null, string $route = null, $action = null)
    {
        $item = new NavItem($title, $route, $action);

        MetronicNavigate::$items[] = $item;

        if (isset($item->items['route'])) {

            $this->includeAfterGroup($item->items['route']);
        }

        return $item;
    }

    /**
     * @return array
     */
    public function get()
    {
        foreach (MetronicNavigate::$items as $key => $item) {
            /** @var Arrayable $item */
            if (!is_array($item)) {
                MetronicNavigate::$items[$key] = $item->toArray();
            } else {
                MetronicNavigate::$items[$key] = $item;
            }
        }

        return MetronicNavigate::$items;
    }

    /**
     * @return array
     */
    public function getMaked()
    {
        return MetronicNavigate::$items;
    }

    /**
     * Register a channel authenticator.
     *
     * @param  string  $channel
     * @param  callable|string  $callback
     * @param  array  $options
     * @return $this
     */
    public function channel($channel, $callback, $options = [])
    {
        \Broadcast::channel($channel, $callback, $options);

        return $this;
    }

    /**
     * @return $this
     */
    public function instance()
    {
        return $this;
    }

    /**
     * @param $name
     * @param $arguments
     */
    public function __call($name, $arguments)
    {
        if (isset(MetronicAdmin::$nav_extensions[$name])) {

            MetronicNavigate::$extension = MetronicAdmin::$nav_extensions[$name];

            MetronicAdmin::$nav_extensions[$name]->navigator($this);

            MetronicNavigate::$extension = null;

            unset(MetronicAdmin::$nav_extensions[$name]);
        }
    }

    /**
     * @param $name
     * @param $to
     */
    protected function includeAfterGroup($name)
    {
        if (is_string($name) && isset(MetronicAdmin::$nav_extensions[$name]) && is_array(MetronicAdmin::$nav_extensions[$name])) {


            foreach (MetronicAdmin::$nav_extensions[$name] as $item) {

                if (!is_array($item)) {

                    MetronicNavigate::$extension = $item;

                    $item->navigator($this);

                    MetronicNavigate::$extension = null;
                }
            }


            unset(MetronicAdmin::$nav_extensions[$name]);
        }
    }
}