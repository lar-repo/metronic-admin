<?php

namespace Lar\MetronicAdmin;

use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider as ServiceProviderIlluminate;
use Lar\Developer\Commands\DumpAutoload;
use Lar\Layout\Layout;
use Lar\Layout\Middleware\LayoutMiddleware;
use Lar\LJS\JaxController;
use Lar\LJS\JaxExecutor;
use Lar\MetronicAdmin\Commands\MetronicControllerCommand;
use Lar\MetronicAdmin\Commands\MetronicDbDumpCommand;
use Lar\MetronicAdmin\Commands\MetronicExtensionCommand;
use Lar\MetronicAdmin\Commands\MetronicGeneratorCommand;
use Lar\MetronicAdmin\Commands\MetronicInstallCommand;
use Lar\MetronicAdmin\Commands\MetronicJaxCommand;
use Lar\MetronicAdmin\Commands\MetronicMixinCommand;
use Lar\MetronicAdmin\Commands\MetronicModalCommand;
use Lar\MetronicAdmin\Commands\MetronicPipeCommand;
use Lar\MetronicAdmin\Commands\MetronicUserCommand;
use Lar\MetronicAdmin\Core\Generators\ExtensionNavigatorHelperGenerator;
use Lar\MetronicAdmin\Core\Generators\FunctionsHelperGenerator;
use Lar\MetronicAdmin\Core\Generators\MacroableHelperGenerator;
use Lar\MetronicAdmin\Exceptions\Handler;
use Lar\MetronicAdmin\Middlewares\Authenticate;

/**
 * Class ServiceProvider
 *
 * @package Lar\Layout
 */
class ServiceProvider extends ServiceProviderIlluminate
{
    /**
     * @var array
     */
    protected $commands = [
        MetronicInstallCommand::class,
        MetronicControllerCommand::class,
        MetronicUserCommand::class,
        MetronicExtensionCommand::class,
        MetronicPipeCommand::class,
        MetronicMixinCommand::class,
        MetronicGeneratorCommand::class,
        MetronicModalCommand::class,
        MetronicJaxCommand::class,
        MetronicDbDumpCommand::class
    ];

    /**
     * Simple bind in app service provider
     * @var array
     */
    protected $bind = [

    ];

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \Lar\MetronicAdmin\Events\Scaffold::class => [
            \Lar\MetronicAdmin\Listeners\Scaffold\CreateMigration::class,
            \Lar\MetronicAdmin\Listeners\Scaffold\CreateModel::class,
            \Lar\MetronicAdmin\Listeners\Scaffold\CreateController::class,
            \Lar\MetronicAdmin\Listeners\Scaffold\CreateControllerPermissions::class,
            \Lar\MetronicAdmin\Listeners\Scaffold\RunMigrate::class,
        ]
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'metronic-auth' => Authenticate::class
    ];

    /**
     * @var ApplicationServiceProvider
     */
    protected $app_provider;

    /**
     * Bootstrap services.
     *
     * @return void
     * @throws \Exception
     */
    public function boot()
    {
        /**
         * Register AdminMetronic Events
         */
        foreach ($this->listen as $event => $listeners) {
            foreach (array_unique($listeners) as $listener) {
                Event::listen($event, $listener);
            }
        }

        /**
         * Register app routes
         */
        if (is_file(metronic_app_path('routes.php'))) {

            \Road::domain(config('metronic.route.domain', ''))
                ->web()
                ->middleware(['metronic-auth'])
                ->lang(config('layout.lang_mode', true))
                ->gets('metronic')
                ->layout(config('metronic.route.layout'))
                ->namespace(metronic_app_namespace('Controllers'))
                ->prefix(config('metronic.route.prefix'))
                ->name(config('metronic.route.name'))
                ->group(metronic_app_path('routes.php'));
        }

        /**
         * Register web routes
         */
        if (is_file(base_path('routes/admin.php'))) {

            \Road::domain(config('metronic.route.domain', ''))
                ->web()
                ->middleware(['metronic-auth'])
                ->lang(config('layout.lang_mode', true))
                ->gets('metronic')
                ->layout(config('metronic.route.layout'))
                ->namespace(metronic_app_namespace('Controllers'))
                ->prefix(config('metronic.route.prefix'))
                ->name(config('metronic.route.name'))
                ->group(base_path('routes/admin.php'));
        }

        /**
         * Register metronic Admin basic routes
         */
        \Road::domain(config('metronic.route.domain', ''))
            ->web()
            ->lang(config('layout.lang_mode', true))
            ->gets('metronic')
            ->middleware(['metronic-auth'])
            ->prefix(config('metronic.route.prefix'))
            ->name(config('metronic.route.name'))
            ->group(__DIR__ . '/routes.php');

        /**
         * Register publishers configs
         */
        $this->publishes([
            __DIR__.'/../config/metronic.php' => config_path('metronic.php')
        ], 'metronic-config');

        /**
         * Register publishers lang
         */
        $this->publishes([
            __DIR__.'/../translations' => resource_path('lang')
        ], 'metronic-lang');

        /**
         * Register publishers assets
         */
        $this->publishes([
            __DIR__ . '/../assets' => public_path('/metronic-admin'),
        ], 'metronic-assets');


        /**
         * Register publishers migrations
         */
        $this->publishes([
            __DIR__.'/../migrations' => database_path('migrations'),
        ], 'metronic-migrations');

        /**
         * Load AdminMetronic views
         */
        $this->loadViewsFrom(__DIR__.'/../views', 'metronic');

        if ($this->app->runningInConsole()) {

            /**
             * Register metronic admin getter for console
             */
            \Get::create('metronic');

            /**
             * Run metronic boots
             */
            MetronicBoot::run();

            /**
             * Helper registration
             */
            DumpAutoload::addToExecute(FunctionsHelperGenerator::class);
            DumpAutoload::addToExecute(ExtensionNavigatorHelperGenerator::class);
            DumpAutoload::addToExecute(MacroableHelperGenerator::class);
        }

        /**
         * Make metronic view variables
         */
        $this->viewVariables();

        /**
         * Register getters
         */
        \Get::register(\Lar\MetronicAdmin\Getters\Menu::class);
        \Get::register(\Lar\MetronicAdmin\Getters\Role::class);
        \Get::register(\Lar\MetronicAdmin\Getters\Functions::class);

        /**
         * Simple bind in service container
         */
        foreach ($this->bind as $key => $item) {
            if (is_numeric($key)) $key = $item;
            $this->app->bind($key, $item);
        }

        /**
         * Run metronic with jax on admin page
         */
        JaxController::on_start(function () {
            $ref = request()->server->get('HTTP_REFERER');
            if ($ref && \Str::is(url(config('metronic.route.prefix')  . "*"), $ref)) {
                MetronicBoot::run();
            }
        });

        /**
         * Register Jax namespace
         */
        \LJS::jaxNamespace(metronic_relative_path('Jax'), metronic_app_namespace('Jax'));
    }

    /**
     * Register services.
     *
     * @return void
     * @throws \Exception
     */
    public function register()
    {
        /**
         * App register provider
         */
        if (class_exists('App\Providers\MetronicServiceProvider')) {

            $this->app->register('App\Providers\MetronicServiceProvider');
        }

        /**
         * Override errors
         */
        $this->app->singleton(
            \Illuminate\Contracts\Debug\ExceptionHandler::class,
            Handler::class
        );

        /**
         * Merge config from having by default
         */
        $this->mergeConfigFrom(
            __DIR__.'/../config/metronic.php', 'metronic'
        );

        /**
         * Register metronic middleware
         */
        $this->registerRouteMiddleware();

        /**
         * Register metronic commands
         */
        $this->commands($this->commands);

        /**
         * Setup auth and disc configuration.
         */
        $this->loadAuthAndDiscConfig();

        /**
         * Register metronic layout
         */
        Layout::registerComponent("metronic_layout", \Lar\MetronicAdmin\Layouts\MetronicLayout::class);

        /**
         * Register metronic Login layout
         */
        Layout::registerComponent("metronic_auth_layout", \Lar\MetronicAdmin\Layouts\MetronicAuthLayout::class);

        /**
         * Register metronic jax executors
         */
        $this->registerJax();
    }

    /**
     * Register jax executors
     */
    protected function registerJax()
    {
        JaxExecutor::addNamespace(__DIR__ . '/Jax', 'Lar\\MetronicAdmin\\Jax');
    }

    /**
     * Register the route middleware.
     *
     * @return void
     */
    protected function registerRouteMiddleware()
    {
        foreach ($this->routeMiddleware as $key => $middleware) {

            app('router')->aliasMiddleware($key, $middleware);
        }
    }

    /**
     * Setup auth and disc configuration.
     *
     * @return void
     */
    private function loadAuthAndDiscConfig()
    {
        config(\Arr::dot(config('metronic.auth', []), 'auth.'));
        config(\Arr::dot(config('metronic.disks', []), 'filesystems.disks.'));
    }

    /**
     * Make metronic view variables
     */
    private function viewVariables()
    {
        app('view')->share([
            'metronic' => config('metronic'),
            'default_page' => config('metronic.paths.view', 'admin').'.page'
        ]);
    }
}

