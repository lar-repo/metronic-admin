<?php

namespace Lar\MetronicAdmin\Interfaces;

/**
 * Interface ActionWorkExtensionInterface
 * @package Lar\MetronicAdmin\Interfaces
 */
interface ActionWorkExtensionInterface {

    /**
     * @return void
     */
    public function handle(): void;
}