<?php

namespace Lar\MetronicAdmin\Interfaces;

use Illuminate\Contracts\Support\Renderable;

/**
 * Interface SegmentContainerInterface
 * @package Lar\MetronicAdmin\Interfaces
 */
interface SegmentContainerInterface extends Renderable {

}