<?php

if (!function_exists('metronic_relative_path')) {

    /**
     * @param  string  $path
     * @return string
     */
    function metronic_relative_path (string $path = "") {

        return "/" . trim("/" . trim(str_replace(base_path(), '', config('metronic.paths.app')), "/")
            . "/" . trim($path, "/"), "/");
    }
}
if (!function_exists('metronic_app_namespace')) {

    /**
     * @param  string  $path
     * @return string
     */
    function metronic_app_namespace (string $path = "") {

        return trim("\\" . trim(config('metronic.app_namespace'), "\\")
            . "\\" . trim($path, "\\"), "\\");
    }
}

if (!function_exists('metronic_related_methods')) {

    /**
     * @param  string  $method
     * @return string[]
     */
    function metronic_related_methods (string $method) {
        if ($method == 'store') { $methods = [$method, 'create', 'access']; }
        else if ($method == 'update') { $methods = [$method, 'edit', 'access']; }
        else if ($method == 'create') { $methods = [$method, 'store', 'access']; }
        else if ($method == 'edit') { $methods = [$method, 'update', 'access']; }
        else if ($method == 'destroy') { $methods = [$method, 'delete', 'access']; }
        else if ($method == 'delete') { $methods = [$method, 'destroy', 'access']; }
        else { $methods = [$method, 'access']; }

        return $methods;
    }
}

if (!function_exists('metronic_controller_can')) {

    /**
     * @param  string|null  $check_method
     * @param  string|null  $check_class
     * @return string
     */
    function metronic_controller_can (string $check_method = null, string $check_class = null) {

        list($class, $method) = \Str::parseCallback(\Route::currentRouteAction());

        if ($check_method) { $method = $check_method; }
        if ($check_class) { $class = $check_class; }
        if (!$class) { return true; }

        $class = trim($class, '\\');
        $ability = "{$class}@{$method}";

        return Gate::has($ability) ? Gate::forUser(admin())->allows($ability) : true;
    }
}

if (!function_exists('metronic_controller_model')) {

    /**
     * @return string
     */
    function metronic_controller_model () {

        $class = \Str::parseCallback(\Route::currentRouteAction())[0];

        if (property_exists($class, 'model')) {

            return $class::$model;
        }

        return "";
    }
}

if (!function_exists('metronic_app_path')) {

    /**
     * @param string $path
     * @return string
     */
    function metronic_app_path (string $path = '') {

        return rtrim(config('metronic.paths.app') . '/' . trim($path, '/'), '/');
    }
}

if ( ! function_exists('metronic_uri') ) {

    /**
     * @param string $uri
     * @return string
     */
    function metronic_uri (string $uri = '') {

        if (!empty($uri)) {

            $uri = "/" . trim($uri, '/');
        }

        return (config('layout.lang_mode') ? '/' . Layout::nowLang() : '') . '/' . trim(config('metronic.route.prefix'), '/') . $uri;
    }
}

if ( ! function_exists('metronic_asset') ) {

    /**
     * @param string $link
     * @return string
     */
    function metronic_asset (string $link = null) {

        if ($link) {

            return asset('metronic-admin/' . trim($link, '/'));
        }

        return asset('metronic-admin');
    }
}

if ( ! function_exists('metronic_user') ) {

    /**
     * @return \Lar\MetronicAdmin\Models\MetronicUser|\App\Models\Admin
     */
    function metronic_user () {

        return MetronicAdmin::user() ?? new \Lar\MetronicAdmin\Models\MetronicUser();
    }
}

if ( ! function_exists('admin') ) {

    /**
     * @return \Lar\MetronicAdmin\Models\MetronicUser|\App\Models\Admin
     */
    function admin () {

        return MetronicAdmin::user() ?? new \Lar\MetronicAdmin\Models\MetronicUser();
    }
}

if ( ! function_exists('metronic_func') ) {

    /**
     * @return \Lar\MetronicAdmin\Core\CheckUserFunction
     */
    function metronic_func () {

        return metronic_user()->func();
    }
}

if ( ! function_exists('versionString') ) {

    /**
     * @param $version
     * @param string $delimiter
     * @return string
     */
    function versionString($version, string $delimiter = '.')
    {
        $version = explode($delimiter, $version);

        $total = count($version);

        foreach ($version as $key => $item) {

            if ($key === ($total-1)) {

                $version[$key] = "<small>{$item}</small>";
            }
        }

        return implode($delimiter, $version);
    }
}


if ( ! function_exists('resource_name') ) {

    /**
     * @param string $append
     * @return string
     */
    function resource_name(string $append = "")
    {
        return preg_replace('/(.*)\.(store|index|create|show|update|destroy|edit)$/', '$1', Route::currentRouteName()) . $append;
    }
}

if ( ! function_exists('makeUrlWithParams') ) {

    /**
     * @param  string  $url
     * @param  array  $params
     * @return string
     */
    function makeUrlWithParams(string $url, array $params)
    {
        $params = http_build_query($params);
        $d = strpos($url, '?') === false ? "?" : "&";
        return $url.$d.$params;
    }
}

if ( ! function_exists('urlWithGet') ) {

    /**
     * @param  array  $params
     * @param  array  $unset
     * @return string
     */
    function urlWithGet(array $params = [], array $unset = [])
    {
        $url = explode('?', url()->current())[0];

        $params = array_merge(request()->query(), $params);

        unset($params['_pjax']);

        foreach ($unset as $item) {
            if (isset($params[$item])) { unset($params[$item]); }
        }

        return $url . (count($params) ? '?' . http_build_query($params) : '');
    }
}


if ( ! function_exists('metronic_model_type') ) {

    /**
     * @param  string|null  $type
     * @return bool|\Lar\MetronicAdmin\Getters\Menu|string|null
     */
    function metronic_model_type(string $type = null) {

        $menu_type = gets()->metronic->menu->type;

        if ($type) {

            return $menu_type === $type;
        }

        return $menu_type;
    }
}

if ( ! function_exists('metronic_model') ) {

    /**
     * @param  string|null  $path
     * @return \Illuminate\Database\Eloquent\Model|\Lar\MetronicAdmin\Getters\Menu|mixed|string|null
     */
    function metronic_model(string $path = null)
    {
        $model = gets()->metronic->menu->model;

        if ($model && $model->exists) {

            if ($path) {

                return multi_dot_call($model, $path);
            }

            return $model;
        }

        return null;
    }
}

if ( ! function_exists('metronic_now') ) {

    /**
     * @return array|\Lar\MetronicAdmin\Getters\Menu|array|null
     */
    function metronic_now()
    {
        return gets()->metronic->menu->now;
    }
}

if ( ! function_exists('array_callable_results') ) {

    /**
     * @param  array  $array
     * @return array
     */
    function array_callable_results(array $array, ...$callable_params)
    {
        foreach ($array as $key => $item) {

            if ($item instanceof Closure) {

                $array[$key] = $item(...$callable_params);
            }
        }

        return $array;
    }
}