<?php

use Lar\Roads\Roads;

/**
 * metronic Auth routes
 */
Road::layout('metronic_auth_layout')->group(function (Roads $roads) {

    $roads->get('/', config('metronic.action.auth.login_form_action'))->name('home');
    $roads->get('login', config('metronic.action.auth.login_form_action'))->name('login');
    $roads->post('login', config('metronic.action.auth.login_post_action'))->name('login.post');
});

/**
 * Basic routes
 */
Road::layout(config('metronic.route.layout'))->group(function (Roads $roads) {

    $roads->get('profile', config('metronic.action.profile.index'))->name('profile');
    $roads->post('profile', config('metronic.action.profile.update'))->name('profile.post');
    $roads->get('profile/logout', config('metronic.action.profile.logout'))->name('profile.logout');
    $roads->post('uploader', config('metronic.action.uploader'))->name('uploader');

    $app_dashboard = "\\" . metronic_app_namespace('Controllers\\DashboardController');

    MetronicNavigate::item('metronic.dashboard', 'dashboard')
        ->action(class_exists($app_dashboard) ? $app_dashboard . "@index" : config('metronic.action.dashboard'))
        ->icon_tachometer_alt();

    $roads->namespace(metronic_app_namespace('Controllers'))->group(function (Roads $roads) {

        \Lar\MetronicAdmin\Core\RoutesAdaptor::create_by_menu($roads);
    });
});
