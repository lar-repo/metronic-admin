<?php

namespace Lar\MetronicAdmin\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Lar\MetronicAdmin\Core\Traits\DumpedModel;

/**
 * Class MetronicPermission
 * @package Lar\MetronicAdmin\Models
 */
class MetronicFunction extends Model
{
    use DumpedModel;
    
    /**
     * @var string
     */
    protected $table = "metronic_functions";

    /**
     * @var string[]
     */
    protected $fillable = [
        "slug", "class", "description", "active"
    ];

    /**
     * @var array
     */
    protected $attributes = [
        'active' => 1
    ];

    /**
     * @var Collection
     */
    static $now;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(MetronicRole::class, "metronic_role_function", "metronic_function_id", "metronic_role_id");
    }

    /**
     * @return array
     */
    public function toDump()
    {
        $functions_array = $this->toArray();

        $functions_array['roles'] = $this->roles->pluck('id')->toArray();

        return $functions_array;
    }
}
