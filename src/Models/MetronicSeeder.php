<?php

namespace Lar\MetronicAdmin\Models;

use Illuminate\Database\Seeder;
use Lar\MetronicAdmin\Commands\MetronicDbDumpCommand;

/**
 * Class MetronicSeeder
 *
 * @package Lar\MetronicAdmin\Models
 */
class MetronicSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (class_exists(MetronicDbDumpCommand::$file_name)) {

            return ;
        }

        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        /** @var MetronicUser $user_model */
        $user_model = config('metronic.auth.providers.metronic.model');

        // create a user.
        $user_model::truncate();

        $user_model::create([
            'login' => 'root',
            'password' => bcrypt('root'),
            'name'     => 'Root',
            'email'    => 'root@root.com'
        ]);

        $user_model::create([
            'login' => 'admin',
            'password' => bcrypt('admin'),
            'name'     => 'Admin',
            'email'    => 'admin@admin.com'
        ]);

        $user_model::create([
            'login' => 'moderator',
            'password' => bcrypt('moderator'),
            'name'     => 'Moderator',
            'email'    => 'moderator@moderator.com'
        ]);

        // create a role.
        MetronicRole::truncate();

        MetronicRole::create([
            'name' => 'Root',
            'slug' => 'root',
        ]);
        MetronicRole::create([
            'name' => 'Administrator',
            'slug' => 'admin',
        ]);
        MetronicRole::create([
            'name' => 'Moderator',
            'slug' => 'moderator',
        ]);

        $user_model::find(1)->roles()->save(MetronicRole::find(1));
        $user_model::find(2)->roles()->save(MetronicRole::find(2));
        $user_model::find(3)->roles()->save(MetronicRole::find(3));

        MetronicPermission::create(['path' => 'admin*', 'method' => ['*'], 'state' => 'close', 'metronic_role_id' => 3]);

        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
