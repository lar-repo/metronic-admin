<?php

namespace Lar\MetronicAdmin\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Lar\MetronicAdmin\Core\Traits\DumpedModel;

/**
 * Class MetronicPermission
 * @package Lar\MetronicAdmin\Models
 */
class MetronicPermission extends Model
{
    use DumpedModel;
    
    /**
     * @var string
     */
    protected $table = "metronic_permission";

    /**
     * @var string[]
     */
    protected $fillable = [
        "path", "method", "state", "description", "metronic_role_id", "active" // state: open, close
    ];

    /**
     * @var string[]
     */
    protected $casts = [
        'method' => 'array'
    ];

    /**
     * @var Collection
     */
    static $now;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function role()
    {
        return $this->hasOne(MetronicRole::class, 'id', 'metronic_role_id');
    }

    /**
     * @return Collection|\Illuminate\Support\Collection|MetronicPermission[]
     */
    public static function now()
    {
        if (static::$now) {

            return static::$now;
        }

        $roles = metronic_user() ? metronic_user()->roles->pluck('id')->toArray() : [1];

        return static::$now = static::whereIn('metronic_role_id', $roles)->where('active', 1)->get();
    }

    /**
     * @param $url
     * @return bool
     */
    public static function checkUrl($url)
    {
        $result = true;

        /** @var MetronicPermission $close */
        foreach (static::now()->where('state', 'close') as $close) {

            $path = static::makeCheckedPath($close->path);

            if (($close->method[0] === '*' || array_search('GET', $close->method) !== false) && \Str::is(url($path), $url)) {

                $result = false;
                break;
            }
        }

        if (!$result) {

            /** @var MetronicPermission $close */
            foreach (static::now()->where('state', 'open') as $open) {

                $path = static::makeCheckedPath($open->path);

                if (($open->method[0] === '*' || array_search('GET', $open->method) !== false) && \Str::is(url($path), $url)) {

                    $result = true;
                    break;
                }
            }
        }

        return $result;
    }

    /**
     * @return bool
     */
    public static function check()
    {
        if (!metronic_user()) {

            return true;
        }

        $result = true;

        $method = request()->ajax() && !request()->pjax() && request()->has("_exec") ? 'POST' : request()->getMethod();

        /** @var MetronicPermission $close */
        foreach (static::now()->where('state', 'close') as $close) {

            $path = static::makeCheckedPath($close->path);

            if (($close->method[0] === '*' || array_search($method, $close->method) !== false) && request()->is($path)) {

                $result = false;
                break;
            }
        }

        if (!$result) {

            /** @var MetronicPermission $close */
            foreach (static::now()->where('state', 'open') as $open) {

                $path = static::makeCheckedPath($open->path);

                if (($open->method[0] === '*' || array_search($method, $open->method) !== false) && request()->is($path)) {

                    $result = true;
                    break;
                }
            }
        }


        return $result;
    }

    /**
     * @param  string  $inner_path
     * @return string
     */
    public static function makeCheckedPath(string $inner_path)
    {
        $per_path = config('layout.lang_mode') ? '*/' : '';

        return trim($per_path.config('metronic.route.prefix'), '/').'/'.trim($inner_path, '/');
    }
}
