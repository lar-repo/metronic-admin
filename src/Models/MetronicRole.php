<?php

namespace Lar\MetronicAdmin\Models;

use Illuminate\Database\Eloquent\Model;
use Lar\MetronicAdmin\Core\Traits\DumpedModel;

/**
 * Class MetronicRole
 *
 * @package Lar\MetronicAdmin\Models
 */
class MetronicRole extends Model
{
    use DumpedModel;

    /**
     * @var string
     */
    protected $table = "metronic_roles";

    /**
     * @var array
     */
    protected $fillable = [
        "name", "slug"
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(config('metronic.auth.providers.metronic.model'), "metronic_role_user", "metronic_role_id", "metronic_user_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function functions()
    {
        return $this->belongsToMany(MetronicFunction::class, "metronic_role_function", "metronic_role_id", "metronic_function_id");
    }
}
