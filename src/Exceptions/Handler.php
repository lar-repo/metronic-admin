<?php

namespace Lar\MetronicAdmin\Exceptions;

use Lar\MetronicAdmin\Layouts\MetronicAuthLayout;
use Lar\MetronicAdmin\Layouts\MetronicLayout;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

/**
 * Class Handler
 * @package Lar\MetronicAdmin\Exceptions
 */
class Handler extends \App\Exceptions\Handler
{
    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($request->is('*/'.config('metronic.route.prefix').'*')) {

            if ($exception instanceof NotFoundHttpException) {

                $layout = new MetronicAuthLayout();
                $layout->setInContent(view('metronic::404'));
                return response($layout->render());
            }

            if (!\MetronicAdmin::guest()) {

                ob_clean();
            }
        }

        return parent::render($request, $exception);
    }
}
