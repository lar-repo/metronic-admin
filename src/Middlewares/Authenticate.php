<?php

namespace Lar\MetronicAdmin\Middlewares;

use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Lar\Layout\Core\LConfigs;
use Lar\Layout\Respond;
use Lar\MetronicAdmin\MetronicBoot;
use Lar\MetronicAdmin\Models\MetronicPermission;

/**
 * Class Authenticate
 *
 * @package Lar\MetronicAdmin\Middlewares
 */
class Authenticate
{
    /**
     * @var Collection
     */
    protected static $menu;

    /**
     * @var bool
     */
    static $access = true;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     *
     * @return mixed
     * @throws \ReflectionException
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::guard('metronic')->guest() && $this->shouldPassThrough($request)) {

            session()->flash("respond", Respond::glob()->toJson());

            return redirect()->route('metronic.dashboard');
        }
        
        if (Auth::guard('metronic')->guest() && !$this->shouldPassThrough($request)) {

            session()->flash("respond", Respond::glob()->toJson());

            $this->unauthenticated($request);
        }

        MetronicBoot::run();

        LConfigs::add('uploader', route('metronic.uploader'));

        if (!$this->access()) {

            if ($request->ajax() && !$request->pjax()) {

                $respond = ["0:toast::error" => [__('metronic.access_denied'), __('metronic.error')]];

                if (request()->has("_exec")) {

                    $respond["1:doc::reload"] = null;
                }

                return response()->json($respond);
            }

            else if (!$request->isMethod('get')) {

                session()->flash("respond", respond()->toast_error([__('metronic.access_denied'), __('metronic.error')])->toJson());

                return back();
            }

            static::$access = false;
        }

        return $next($request);
    }

    /**
     * @return bool
     */
    protected function access()
    {
        $now = metronic_now();

        list($class, $method) = \Str::parseCallback(\Route::currentRouteAction(), 'index');
        $classes = [trim($class, "\\")];

        if ($now && isset($now['extension']) && $now['extension']) {
            $classes[] = get_class($now['extension']);
        }

        if (!metronic_controller_can()) {

            return false;
        }

        if (isset($now['roles']) && !metronic_user()->hasRoles($now['roles'])) {

            return false;
        }

        return MetronicPermission::check();
    }

    /**
     * Determine if the request has a URI that should pass through verification.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     */
    protected function shouldPassThrough($request)
    {
        $excepts = [
            metronic_uri('login'),
            metronic_uri('logout'),
        ];

        foreach ($excepts as $except) {

            if ($except !== '/') {

                $except = trim($except, '/');
            }

            if ($request->is($except)) {

                return true;
            }
        }

        return false;
    }

    /**
     * @param  Request  $request
     * @throws AuthenticationException
     */
    protected function unauthenticated(Request $request)
    {
        $all = $request->all();
        if ($request->has('_pjax')) { unset($all['_pjax']); }
        $url = url()->current() . (count($all) ? "?" . http_build_query($all) : "");
        session(['return_authenticated_url' => $url]);

        throw new AuthenticationException(
            'Unauthenticated.', ['metronic'], route('metronic.login')
        );
    }
}
