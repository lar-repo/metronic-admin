<?php

namespace Lar\MetronicAdmin\Getters;

use Lar\Developer\Getter;

/**
 * Class Menu
 * 
 * @package Lar\MetronicAdmin\Getters
 */
class Role extends Getter
{
    /**
     * @var string
     */
    public static $name = "metronic.role";

    public static function functions()
    {

    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function default()
    {
        return collect([]);
    }
}
