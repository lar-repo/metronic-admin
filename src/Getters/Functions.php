<?php

namespace Lar\MetronicAdmin\Getters;

use Lar\Developer\Getter;
use Lar\MetronicAdmin\Models\MetronicFunction;

/**
 * Class Menu
 * 
 * @package Lar\MetronicAdmin\Getters
 */
class Functions extends Getter
{
    /**
     * @var string
     */
    public static $name = "metronic.functions";

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|\Lar\MetronicAdmin\Models\MetronicFunction[]
     */
    public static function list()
    {
        return MetronicFunction::where('active',1)->get();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function default()
    {
        return collect([]);
    }
}
