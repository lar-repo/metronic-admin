<?php

namespace Lar\MetronicAdmin\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Lar\MetronicAdmin\Models\MetronicRole;

/**
 * Class MetronicRoleResource
 * @package Lar\MetronicAdmin\Resources
 * @mixin MetronicRole
 */
class MetronicRoleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'name' => $this->name
        ];
    }
}
