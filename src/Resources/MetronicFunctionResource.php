<?php

namespace Lar\MetronicAdmin\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Lar\MetronicAdmin\Models\MetronicFunction;

/**
 * Class MetronicFunctionResource
 * @package Lar\MetronicAdmin\Resources
 * @mixin MetronicFunction
 */
class MetronicFunctionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'slug' => $this->slug,
            'class' => $this->class,
            'description' => lang_in_text($this->description),
            'roles' => MetronicRoleResource::collection($this->roles)->toArray($request)
        ];
    }
}
