<?php

namespace Lar\MetronicAdmin\Layouts;

use Lar\Layout\Abstracts\Component;
use Lar\Layout\Tags\DIV;
use Lar\MetronicAdmin\Components\Vue\Navigator;
use Lar\MetronicAdmin\Middlewares\Authenticate;
use Lar\MetronicAdmin\Segments\AccessDenied;

/**
 * Landing Class
 *
 * @package App\Layouts
 */
class MetronicLayout extends MetronicBase
{
    /**
     * To enable the module, specify the container identifier in the parameter.
     *
     * @var bool|string
     */
    protected $pjax = "kt_wrapper";

    /**
     * MetronicLayout constructor.
     *
     * @param string $body_class
     * @throws \Exception
     */
    public function __construct()
    {
        parent::__construct();

        $this->body->addClass('header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading');
        $this->body->attr('id', 'kt_body');

        try {

            $this->body->view('metronic::layout.mobile_nav');

            $this->body->div(['d-flex flex-column flex-root'], function (DIV $div) {

                $div->div(['d-flex flex-row flex-column-fluid page'], function (DIV $div) {

                    $div->view('metronic::layout.nav2');

                    $div->div(['d-flex flex-column flex-row-fluid wrapper', 'id' => 'kt_wrapper'], function (DIV $div) {

                        $div->view('metronic::layout.header');

                        $div->div(['content d-flex flex-column flex-column-fluid', 'id' => 'kt_content'], function (DIV $div) {

                            $div->view('metronic::layout.container_header');

                            $this->toComponent($div, 'prep_end_wrapper');

                            $div->div(['d-flex flex-column-fluid'])
                                ->div(['container'])
                                ->haveLink($this->container);

                            $this->toComponent($this->container, 'prep_end_content');

                            $this->toComponent($div, 'app_end_wrapper');
                        });

                        $div->view('metronic::layout.footer');
                    });
                });

//                    $this->toComponent($div, 'prep_end_wrapper');
//
//                    $div->section(['content', 'id' => 'metronic-content-container'])
//                        ->haveLink($this->container);
//
//                    $this->toComponent($this->container, 'prep_end_content');
//
//                    $this->toComponent($div, 'app_end_wrapper');
            });

            $this->body->view('metronic::layout.scrolltop');

        } catch (\Exception $exception) {
            dd($exception);
        }
    }

    /**
     * Add data in to layout content
     *
     * @param $data
     * @return void
     * @throws \Exception
     */
    public function setInContent($data)
    {
        try {

            if (Authenticate::$access) {

                $this->container->appEnd($data);

                $this->toComponent($this->container, 'app_end_content');
            }

            else {

                $this->container->appEnd(AccessDenied::create());
            }

        } catch (\Exception $exception) {

            dd($exception);
        }
    }

    /**
     * @param  Component  $component
     * @param  string  $segment
     */
    private function toComponent(Component $component, string $segment)
    {
        foreach (\MetronicAdmin::getSegments($segment) as $segment) {
            if (\View::exists($segment['component'])) {
                $component->view($segment['component'], $segment['params']);
            } else {
                $component->appEnd(new $segment['component'](...$segment['params']));
            }
        }
    }
}
