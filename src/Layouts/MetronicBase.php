<?php

namespace Lar\MetronicAdmin\Layouts;

use Lar\Layout\Abstracts\LayoutComponent;
use Lar\Layout\Core\LConfigs;
use Lar\MetronicAdmin\MetronicAdmin;

/**
 * Landing Class
 *
 * @package App\Layouts
 */
class MetronicBase extends LayoutComponent
{
    /**
     * Protected variable Name
     *
     * @var string
     */
    protected $name = "metronic_layout";

    /**
     * @var string
     */
    protected $default_title = "MetronicAdmin";

    /**
     * @var array
     */
    protected $head_styles = [
        'ljs' => [
            'fancy'
        ],

        'metronic-admin/plugins/fontawesome-free/css/all.min.css',

        'metronic-admin/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css',
        'metronic-admin/plugins/bootstrap-slider/css/bootstrap-slider.min.css',
        'metronic-admin/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css',
        'metronic-admin/plugins/ekko-lightbox/ekko-lightbox.css',
        'metronic-admin/plugins/flag-icon-css/css/flag-icon.min.css',
        'metronic-admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css',
        'metronic-admin/plugins/ion-rangeslider/css/ion.rangeSlider.min.css',
        'metronic-admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css',
//        'metronic-admin/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css',
        'metronic-admin/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css',
        'metronic-admin/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css',
        'metronic-admin/plugins/daterangepicker/daterangepicker.css',

        'metronic-admin/plugins/bootstrap-fileinput/css/fileinput.min.css',
        'metronic-admin/plugins/bootstrap-fileinput/themes/explorer-fas/theme.css',
        'metronic-admin/plugins/bootstrap-iconpicker-1.10.0/dist/css/bootstrap-iconpicker.min.css',
        'metronic-admin/plugins/nestable/jquery.nestable.css',
        'metronic-admin/plugins/editor.md-master/css/editormd.min.css',
        'metronic-admin/plugins/bootstrap4-editable/css/bootstrap-editable.css',
        'metronic-admin/plugins/codemirror/lib/codemirror.css',
        'metronic-admin/plugins/star-rating/star-rating.min.css',
        'metronic-admin/plugins/star-rating/krajee-fas/theme.min.css',

        'metronic-admin/plugins/custom/fullcalendar/fullcalendar.bundle.css',
        'metronic-admin/plugins/global/plugins.bundle.css',
        'metronic-admin/plugins/custom/prismjs/prismjs.bundle.css',


        'metronic-admin/css/style.bundle.css',
        'metronic-admin/css/themes/layout/header/base/light.css',
        'metronic-admin/css/themes/layout/header/menu/light.css',
        'metronic-admin/css/themes/layout/brand/dark.css',
        'metronic-admin/css/themes/layout/aside/dark.css',
        'https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700',
        'metronic-admin/css/app.css',
    ];

    /**
     * @var array
     */
    protected $body_scripts = [
        'metronic-admin/plugins/jquery/jquery.min.js',
        'metronic-admin/plugins/bootstrap/js/bootstrap.bundle.min.js',

        'metronic-admin/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js',
        'metronic-admin/plugins/bootstrap-slider/bootstrap-slider.min.js',
        'metronic-admin/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        'metronic-admin/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js',
        'metronic-admin/plugins/bs-custom-file-input/bs-custom-file-input.min.js',
        'metronic-admin/plugins/ekko-lightbox/ekko-lightbox.min.js',
        'metronic-admin/plugins/fastclick/fastclick.js',
        'metronic-admin/plugins/ion-rangeslider/js/ion.rangeSlider.min.js',
        'metronic-admin/plugins/jquery-knob/jquery.knob.min.js',
        'metronic-admin/plugins/jquery-mousewheel/jquery.mousewheel.js',
//        'metronic-admin/plugins/moment/moment.min.js',
        'metronic-admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js',
        'metronic-admin/plugins/popper/umd/popper.min.js',
        'metronic-admin/plugins/popper/umd/popper-utils.min.js',
        'metronic-admin/plugins/sparklines/sparkline.js',
        'metronic-admin/plugins/daterangepicker/moment.min.js',
        'metronic-admin/plugins/daterangepicker/daterangepicker.js',
        'metronic-admin/plugins/sweetalert2/sweetalert2.min.js',

        'metronic-admin/plugins/jquery-validation/jquery.validate.min.js',
        'metronic-admin/plugins/jquery-validation/localization/messages_ru.min.js',
        'metronic-admin/plugins/bootstrap-fileinput/js/plugins/piexif.min.js',
        'metronic-admin/plugins/bootstrap-fileinput/js/plugins/sortable.min.js',
        'metronic-admin/plugins/ckeditor5-build-classic/ckeditor.js',
        'metronic-admin/plugins/bootstrap-fileinput/js/fileinput.min.js',
        'metronic-admin/plugins/bootstrap-fileinput/themes/explorer-fas/theme.js',
        'metronic-admin/plugins/bootstrap-fileinput/themes/fas/theme.js',
        'metronic-admin/plugins/bootstrap-fileinput/js/locales/ru.js',
        'metronic-admin/plugins/bootstrap-iconpicker-1.10.0/dist/js/bootstrap-iconpicker.bundle.min.js',
        'metronic-admin/plugins/bootstrap-number-input.js',
        'metronic-admin/plugins/nestable/jquery.nestable.js',
        'metronic-admin/plugins/editor.md-master/editormd.min.js',
        'metronic-admin/plugins/editor.md-master/languages/en.js',

        'metronic-admin/plugins/bootstrap4-editable/js/bootstrap-editable.min.js',
        'metronic-admin/plugins/codemirror/lib/codemirror.js',
        'metronic-admin/plugins/codemirror/addon/selection/selection-pointer.js',
        'metronic-admin/plugins/codemirror/addon/edit/matchbrackets.js',
        'metronic-admin/plugins/codemirror/addon/comment/continuecomment.js',
        'metronic-admin/plugins/codemirror/addon/comment/comment.js',
        'metronic-admin/plugins/codemirror/mode/xml/xml.js',
        'metronic-admin/plugins/codemirror/mode/javascript/javascript.js',
        'metronic-admin/plugins/codemirror/mode/css/css.js',
        'metronic-admin/plugins/codemirror/mode/vbscript/vbscript.js',
        'metronic-admin/plugins/codemirror/mode/htmlmixed/htmlmixed.js',
        'metronic-admin/plugins/codemirror/mode/markdown/markdown.js',
        'metronic-admin/plugins/star-rating/star-rating.min.js',
        'metronic-admin/plugins/star-rating/locales/ru.js',
        'metronic-admin/plugins/star-rating/krajee-fas/theme.min.js',

        'ljs' => [
            'jq', 'alert', 'nav', 'mask', 'select2', 'fancy'
        ],

        'metronic-admin/plugins/ptty.jquery.js',
        'metronic-admin/plugins/global/plugins.bundle.js',
        'metronic-admin/plugins/custom/prismjs/prismjs.bundle.js',
//        'metronic-admin/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js',
        'metronic-admin/js/scripts.bundle.js',
        'metronic-admin/plugins/custom/fullcalendar/fullcalendar.bundle.js',
        'metronic-admin/js/pages/widgets.js',

        'metronic-admin/js/app.js',
    ];

    /**
     * @var array
     */
    protected $metas = [
        ['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1']
    ];

    /**
     * MetronicBase constructor.
     */
    public function __construct()
    {
        if (MetronicAdmin::$echo) {
            $this->body_scripts['ljs'][] = 'echo';
        }

        $this->body_scripts['ljs'][] = 'vue';

        foreach (\MetronicAdmin::extensions() as $extension) {

            $this->body_scripts = array_merge_recursive($this->body_scripts, $extension->config()->getScripts());

            $this->head_styles = array_merge_recursive($this->head_styles, $extension->config()->getStyles());
        }

        $this->body_scripts = array_merge($this->body_scripts, config('metronic.body_scripts', []));

        $this->js()->state('admin', \MetronicAdmin::user());

        parent::__construct();

        $this->head->link(['rel' => 'shortcut icon', 'href' => asset('metronic-admin/media/logos/favicon.ico')]);
        $this->head->link(['rel' => 'apple-touch-icon', 'href' => asset('metronic-admin/media/logos/favicon.ico')]);
    }

    /**
     * @throws \Exception
     */
    protected function assetInjects()
    {
        $text = <<<HTML
<!--end::Demo Panel-->
<script>var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";</script>
<!--begin::Global Config(global config for global JS scripts)-->
<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1400 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#3699FF", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#E4E6EF", "dark": "#181C32" }, "light": { "white": "#ffffff", "primary": "#E1F0FF", "secondary": "#EBEDF3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#3F4254", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#EBEDF3", "gray-300": "#E4E6EF", "gray-400": "#D1D3E0", "gray-500": "#B5B5C3", "gray-600": "#7E8299", "gray-700": "#5E6278", "gray-800": "#3F4254", "gray-900": "#181C32" } }, "font-family": "Poppins" };</script>
HTML;

        $this->head->appEnd($text);

        parent::assetInjects(); // TODO: Change the autogenerated stub
    }
}
