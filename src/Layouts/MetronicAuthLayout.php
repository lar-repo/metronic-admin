<?php

namespace Lar\MetronicAdmin\Layouts;

/**
 * Landing Class
 *
 * @package App\Layouts
 */
class MetronicAuthLayout extends MetronicBase
{
    /**
     * Protected variable Name
     *
     * @var string
     */
    protected $name = "metronic_auth_layout";

    /**
     * @var string
     */
    protected $pjax = "metronic-login-container";

    /**
     * MetronicAuthLayout constructor.
     *
     * @throws \Exception
     */
    public function __construct()
    {
        $this->head_styles[] = "metronic-admin/css/pages/login/login-1.css";
        parent::__construct();
        $this->body->addClass('header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading');
        $this->body->attr('id', 'kt_body');
    }
}
