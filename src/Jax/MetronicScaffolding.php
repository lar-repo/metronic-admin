<?php

namespace Lar\MetronicAdmin\Jax;

use Lar\LJS\JaxExecutor;
use Lar\MetronicAdmin\Events\Scaffold;

/**
 * Class MetronicScaffolding
 * @package Lar\MetronicAdmin\Jax
 */
class MetronicScaffolding extends MetronicAdminExecutor
{
    /**
     * @return bool
     */
    public function access()
    {
        return parent::access() && \MetronicAdmin::user()->isRoot();
    }

    /**
     * @param  array  $data
     */
    public function __invoke(array $data)
    {
        event(new Scaffold($data));
    }
}