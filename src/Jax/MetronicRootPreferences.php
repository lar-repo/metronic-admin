<?php

namespace Lar\MetronicAdmin\Jax;

use Lar\MetronicAdmin\Models\MetronicFunction;
use Lar\MetronicAdmin\Resources\MetronicFunctionResource;

/**
 * Class MetronicAdmin
 * @package App\Http\JaxExecutors
 */
class MetronicRootPreferences extends MetronicAdmin
{
    /**
     * Public method access
     * 
     * @return bool
     */
    public function access() {
        
        return parent::access() && \MetronicAdmin::user()->isRoot();
    }

    /**
     * @param  array  $funcs
     * @param  string  $class
     * @return array
     */
    public function update_functions(array $funcs, string $class)
    {
        foreach ($funcs as $func) {

            if (isset($func['id'])) {
                /** @var MetronicFunction $f */
                $f = MetronicFunction::find($func['id']);
                $f->update([
                        'description' => $func['description'],
                        'slug' => $func['slug']
                    ]);
            }

            else {
                /** @var MetronicFunction $f */
                $f = MetronicFunction::create([
                    'description' => $func['description'],
                    'slug' => $func['slug'],
                    'class' => trim($class, '\\')
                ]);
            }
            $f->roles()->sync(collect($func['roles'])->pluck('id')->toArray());
        }

        return MetronicFunctionResource::collection(
            MetronicFunction::with('roles')->where('class', trim($class, '\\'))->get()
        )->toArray(request());
    }

    /**
     * @param  int  $id
     * @param $class
     * @return array
     * @throws \Exception
     */
    public function drop_function(int $id, $class)
    {
        $func = MetronicFunction::find($id);

        if ($func) {
            $func->delete();
        }

        return MetronicFunctionResource::collection(
            MetronicFunction::with('roles')->where('class', trim($class, '\\'))->get()
        )->toArray(request());
    }
}
