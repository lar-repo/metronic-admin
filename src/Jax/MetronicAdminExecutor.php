<?php

namespace Lar\MetronicAdmin\Jax;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Lar\LJS\JaxExecutor;
use Lar\MetronicAdmin\Controllers\ModalController;
use Lar\MetronicAdmin\MetronicBoot;
use Lar\MetronicAdmin\Models\MetronicFunction;
use Lar\MetronicAdmin\Resources\MetronicFunctionResource;

/**
 * Class MetronicAdminExecutor
 * @package Lar\MetronicAdmin\Jax
 */
class MetronicAdminExecutor extends JaxExecutor
{
    /**
     * Public method access
     * 
     * @return bool
     */
    public function access() {
        
        return !\MetronicAdmin::guest();
    }
}
